# With Gradient boosting - giving best results yet

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
pd.options.display.width = 2000

prop = pd.read_csv('data/properties_2016.csv')
#Calculaing
prop.loc[prop.roomcnt==0,'roomcnt'] = prop.bedroomcnt +prop.calculatedbathnbr
prop['additional_room'] = prop.roomcnt - (prop.bedroomcnt +prop.calculatedbathnbr)
prop['total_rooms'] = prop.additional_room + (prop.bedroomcnt +prop.calculatedbathnbr)
prop['landtax_to_lotsize']=prop.landtaxvaluedollarcnt/(prop.lotsizesquarefeet +1.0)
prop['structuretax_to_lotsize']= prop.structuretaxvaluedollarcnt/(prop.lotsizesquarefeet +1.0)
prop['taxamount_to_lotsize']=prop.taxamount/(prop.lotsizesquarefeet +1.0)
prop['lotsize_to_totalrooms']= prop.lotsizesquarefeet/(prop.total_rooms +1.0)
prop['calculatedfinishedsquarefeet_to_totalrooms'] = prop.calculatedfinishedsquarefeet/(prop.total_rooms + 1)
prop['structuretax_to_calculatedfinishedsquarefeet']= prop.structuretaxvaluedollarcnt/(prop.calculatedfinishedsquarefeet +1.0)
prop['lotsize_minus_calculatedfinishedsquarefeet'] = prop.lotsizesquarefeet - prop.calculatedfinishedsquarefeet
prop['taxamount_to_taxvaluedollarcnt'] = prop.taxamount/(prop.taxvaluedollarcnt +1)

train = pd.read_csv("data/train_2016_v2.csv",parse_dates=['transactiondate'])
train['month'] =  train['transactiondate'].apply(lambda x: x.month)

def fillna_encode(prop):
    for c in prop.columns:
        prop[c] = prop[c].fillna(0)
        if prop[c].dtype == 'object':
            lbl = LabelEncoder()
            lbl.fit(list(prop[c].values))
            prop[c] = lbl.transform(list(prop[c].values))

fillna_encode(prop)

train_df = train.merge(prop, how='left', on='parcelid')
cols = train_df.columns
cols_input = cols.drop(['parcelid', 'logerror', 'transactiondate','month'])

x_train = train_df.query('month <6 and abs(logerror) <=0.4')
y_train = train_df.query('month <6 and abs(logerror) <=0.4')["logerror"].values.astype(np.float32)
x_test = train_df.query('month ==6')
y_test = train_df.query('month ==6')["logerror"].values.astype(np.float32)

from sklearn.ensemble import GradientBoostingRegressor as GBR

# For outliers dataset
#gbr = GBR(loss='lad',  min_samples_leaf=50,max_depth=5,n_estimators=150)
gbr = GBR(loss='huber',alpha=0.1,  min_samples_leaf=50,max_depth=5,n_estimators=150)
gbr.fit(x_train[cols_input], y_train)

x_test['result'] = gbr.predict(x_test[cols_input])

#Base result: 0.065555229888043298 - with abs(logerror) <=0.3
#0.065466586240686742
mean_absolute_error(x_test['logerror'],x_test['result'])

imp = pd.DataFrame(data={'features':cols_input,'importance':gbr.feature_importances_})
imp.sort_values(by='importance')

#Submission
prop_result = gbr.predict(prop[cols_input])

sub=pd.DataFrame()
sub['ParcelId'] = prop['parcelid']
sub['201610'] = [float(format(x, '.4f')) for x in prop_result]
sub['201611'] = sub['201610']
sub['201612'] = sub['201610']
sub['201710'] = 0
sub['201711'] = 0
sub['201712'] = 0

sub.to_csv('ana/results7.csv',index=False)