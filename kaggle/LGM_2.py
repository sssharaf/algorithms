import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features as features
from importlib import reload
import gc
from types import SimpleNamespace
from sklearn.linear_model import Ridge,LinearRegression
from sklearn.preprocessing import PolynomialFeatures

def create_est(groupKey, x_train, x_val, cols_input, cat_features):
    z_dic = {}
    grp_train_df = x_train.groupby(by=groupKey).groups
    for z, idx in grp_train_df.items():
        print("Processing for %s %s" % (groupKey, z))
        z_test_df = x_val.loc[x_val[groupKey] == z]

        if x_train.loc[x_train.index.isin(idx)].size > 1:
            lgmr = LGBMRegressor(n_estimators=300, objective='mae', reg_alpha=2, max_bin=512 \
                                 , min_child_samples=50, num_leaves=31)

            if z_test_df.size > 1:
                lgmr.fit(x_train.loc[x_train.index.isin(idx), cols_input],
                         x_train.loc[x_train.index.isin(idx), 'logerror'],
                         categorical_feature=cat_features \
                         , eval_set=(z_test_df[cols_input], z_test_df['logerror']) \
                         , eval_metric='mae', early_stopping_rounds=20)
            else:
                lgmr.fit(x_train.loc[x_train.index.isin(idx), cols_input],
                         x_train.loc[x_train.index.isin(idx), 'logerror'],
                         categorical_feature=cat_features)

            ns = SimpleNamespace()
            ns.estimator = lgmr
            z_dic[z] = ns
    return z_dic

def use_linear_reg (mm,prop,mm_group_key='regionidzip',mm_stat_feature='regionidzip_mean_logerror'):
    mm_for_lr = mm.reset_index().set_index(mm_group_key)
    z_to_lr_dic = {}
    #new_col_name="%s_%s_logerror"%(mm_group_key,mm_stat_feature)
    new_col_name=mm_stat_feature
    for z in prop[mm_group_key].unique():
        if z in mm_for_lr.index:
            print("Processing %s %s" % (mm_group_key,z))
            trn_data = mm_for_lr.loc[mm_for_lr.index.isin([z]),:]
            lr = LinearRegression()
            lr.fit(trn_data['month'].values.reshape(-1, 1), trn_data[mm_stat_feature].values.reshape(-1, 1))
            z_to_lr_dic[z] = lr
            prop.loc[prop[mm_group_key]==z,new_col_name] = \
                lr.predict(prop.loc[prop[mm_group_key] == z, 'month'].values.reshape(-1, 1))

            gc.collect()
    return prop

def create_est2(groupKey, x_train, x_val, x_test, cols_input, cat_features,sample_weight=None):
    z_dic = {}
    new_col_name = 'p_{0}'.format(groupKey)
    x_test[new_col_name]=np.nan
    #grp_train_df = x_train.groupby(by=groupKey).groups
    for z in x_train[groupKey].unique():
        print("Processing for %s %s" % (groupKey, z))

        lgmr=_predict(z,groupKey, new_col_name,x_train, x_val, x_test, cols_input, cat_features,sample_weight=None)

        x_test.loc[x_test[groupKey] == z, new_col_name] = \
            lgmr.predict(x_test.loc[x_test[groupKey] == z, cols_input])

        #ns = SimpleNamespace()
        #ns.estimator = lgmr
        #z_dic[z] = ns

        gc.collect()
    return z_dic

def _predict(z,groupKey,new_col_name,x_train,x_test,x_val,cols_input,cat_features,sample_weight=None):
    lgmr = LGBMRegressor(n_estimators=200, objective='mae', reg_alpha=2, max_bin=128 \
                         , min_child_samples=50, num_leaves=31,subsample=0.8)

    if sample_weight is None:
        sample_weight = x_train.apply(lambda x: 1.0*x['month'] if x[groupKey] == z else 0.5*x['month'],axis=1)

    lgmr.fit(x_train[cols_input],x_train['logerror'],sample_weight=sample_weight)

    lgmr.fit(x_train.loc[:, cols_input],
             x_train.loc[:, 'logerror'],
             sample_weight=sample_weight,
             categorical_feature=cat_features \
             , eval_set=(x_val[cols_input], x_val['logerror']) \
             , eval_metric='mae', early_stopping_rounds=80)

    # x_test.loc[x_test[groupKey] == z, new_col_name] = \
    #     lgmr.predict(x_test.loc[x_test[groupKey] == z, cols_input])

    del sample_weight
    return lgmr


def predict(x_test,zip_dic,group_key,cols_input):
    x_test[ 'p_{0}'.format(group_key)]=np.nan
    for z, ns in zip_dic.items():
        estimator = ns.estimator
        test_idx = x_test.loc[x_test[group_key] == z].index
        print("Predicting for %s %s with size %s"%(group_key,z,test_idx.size))
        if test_idx.size > 0:
            x_test.loc[(x_test.index.isin(test_idx)), 'p_{0}'.format(group_key)] \
                = estimator.predict(x_test.loc[(x_test.index.isin(test_idx)), cols_input])
