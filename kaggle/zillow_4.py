# Add near-by houses as columns - doesn't work
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
#from xgboost import  XGBRegressor as XGBR
from sklearn.neighbors import LSHForest as LSH
from sklearn.ensemble import GradientBoostingRegressor as GBR
from sklearn.neighbors import NearestNeighbors as NN
from types import SimpleNamespace

pd.options.display.width = 2000

prop = pd.read_csv('data/properties_2016.csv')
train = pd.read_csv("data/train_2016_v2.csv",parse_dates=['transactiondate'])
train['month'] =  train['transactiondate'].apply(lambda x: x.month)
imp_features = pd.read_csv('ana/feature_importance.csv')
imp_features_cols = imp_features.query('importance ==1')['features']
imp_features_cols = imp_features_cols.tolist()

for c in prop.columns:
    prop[c] = prop[c].fillna(-1)
    if prop[c].dtype == 'object':
        lbl = LabelEncoder()
        lbl.fit(list(prop[c].values))
        prop[c] = lbl.transform(list(prop[c].values))

for c in prop.columns:
    if column_types[c] == 'category':
        prop[c]=prop[c].astype('category')

train_df = train.merge(prop, how='left', on='parcelid')
cols = train_df.columns
cols_input = cols.drop(['parcelid', 'logerror', 'transactiondate','month'])

x_train_all_cols = train_df.query('month <6 and abs(logerror) <=0.3')
x_train = train_df.query('month <6 and abs(logerror) <=0.3')[cols_input]
y_train = train_df.query('month <6 and abs(logerror) <=0.3')["logerror"].values.astype(np.float32)
x_test = train_df.query('month ==6')
y_test = train_df.query('month ==6')["logerror"].values.astype(np.float32)

# add features
#cols_feature_add = ['latitude','longitude','calculatedfinishedsquarefeet','lotsizesquarefeet','structuretaxvaluedollarcnt','landtaxvaluedollarcnt']
cols_feature_add = imp_features_cols
nn= NN(algorithm='ball_tree')
nn.fit(x_train[cols_feature_add])
d,i =nn.kneighbors(x_train[cols_feature_add])

x_train = x_train.reset_index()

new_cols_by_index={}
for counter in range(0,5):
    new_cs = [c + '_' + str(counter) for c in cols_feature_add]
    new_cols_by_index[counter]=new_cs

for i_r_count,i_r in enumerate(i):
    #x_train_r = x_train.iloc[i_r]
    if (np.mod(i_r_count,1000)):
        print("Completed %s"%(i_r_count))
    for i_r1_count,i_r1 in enumerate(i_r):
        if i_r1_count == 0: continue
        #print(i_r_count)
        #print(i_r1)
        nearest_r= x_train.loc[i_r1]
        for c,nc in zip(cols_feature_add,new_cols_by_index[i_r1_count]):
            x_train.loc[i_r_count,nc]=nearest_r[c]
    #     print(x_train.loc[i_r_count])
    #     #break
    # if i_r_count > 4:
    #     break


d_test,i_test =nn.kneighbors(x_test[cols_feature_add])
x_test_1 = x_test[cols_input].reset_index()
for i_r_count,i_r in enumerate(i_test):
    if (np.mod(i_r_count,1000)):
        print("Completed %s"%(i_r_count))
    for i_r1_count,i_r1 in enumerate(i_r):
        if i_r1_count == 0: continue
        nearest_r= x_train.loc[i_r1]
        for c,nc in zip(cols_feature_add,new_cols_by_index[i_r1_count]):
            x_test_1.loc[i_r_count,nc]=nearest_r[c]

gbr = GBR(loss='ls',  min_samples_leaf=50,max_depth=3,n_estimators=200,subsample=0.8)
gbr.fit(x_train, y_train)

x_test_1.drop(labels='index',axis=1,inplace=True)
x_train.drop(labels='index',axis=1,inplace=True)

x_test['result'] = gbr.predict(x_test_1)

#Base result: Out[40]: 0.065840523047375643 - with abs(logerror) <=0.3
mean_absolute_error(x_test['logerror'],x_test['result'])

imp = pd.DataFrame(data={'features':x_test_1.columns,'importance':gbr.feature_importances_})
imp.sort_values(by='importance')

#Submission
prop_result_df = prop.copy()
prop_result_df['result'] = gbr.predict(prop[cols_input])

sub=pd.DataFrame()
sub['ParcelId'] = prop_result_df['parcelid']
pred=prop_result_df['result']
sub['201610'] = [float(format(x, '.4f')) for x in pred]
sub['201611'] = sub['201610']
sub['201612'] = sub['201610']
sub['201710'] = 0
sub['201711'] = 0
sub['201712'] = 0

sub.to_csv('ana/results7.csv',index=False)