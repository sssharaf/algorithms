
import numpy as np
import pandas as pd
from mlens.externals.sklearn.base import BaseEstimator
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features2 as features2
import kaggle.features3 as features3
import kaggle.features4 as features4
import kaggle.features5 as features5
from importlib import reload
import gc
from types import SimpleNamespace
import kaggle.LGM_2 as helper
import kaggle.utils as utils
import pickle
from sklearn.linear_model import Ridge,LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import kaggle.alterable_features as alt_feature
from catboost import CatBoostRegressor
from mlxtend.regressor import StackingRegressor
from sklearn.externals import joblib
from mlens.ensemble import Subsemble,BlendEnsemble

class ColumnPartitioner(BaseEstimator):

    def __init__(self, col_num):
        self.col_num = col_num

    def fit(self,X,y):
        print("Shape of X")
        print(np.shape(X))
        #print(X[:,-9:])
        #self.X = X


    def predict(self,X):
     #   return X.loc[self.col_num]
        r= X[:,self.col_num]
        #print(r)
        return r

class LogErrorPartitioner(BaseEstimator):

    def __init__(self, logerror):
        self.logerror = logerror

    def fit(self,X,y):
        self.y = abs(y)

    def predict(self,X):
        ys = self.y < self.logerror
        return [ 1 if y else 0 for y in ys]
