# With Gradient boosting - giving best results yet

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features2 as features2
import kaggle.features3 as features3
import kaggle.features4 as features4
import kaggle.features5 as features5
import kaggle.features6 as features6
from importlib import reload
import gc
from types import SimpleNamespace
import kaggle.LGM_2 as helper
import kaggle.utils as utils
import pickle
from sklearn.linear_model import Ridge,LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import kaggle.alterable_features as alt_feature
from catboost import CatBoostRegressor
pd.options.display.width = 2000
pd.options.display.max_columns=100

class LGM2_2_2(object):

    def load_prop(self):
        prop = pickle.load(open('data/pp_properties_updated_2016.pickle', 'rb'))
        prop.fillna(0, inplace=True)
        self.prop = prop

    def create_features(self,version=3):
        prop = self.prop
        if version ==2:
            reload(features2)
            features2.create_features(prop)
        elif version ==3:
            reload(features3)
            features3.create_features(prop)
        elif version == 4:
            reload(features4)
            features4.create_features(prop)
        elif version == 5:
            reload(features5)
            features5.create_features(prop)
        elif version == 6:
            reload(features6)
            features6.create_features(prop)
        else :
            raise BaseException("Not supported")

    def prepare_train_df(self):

        prop = self.prop
        train = pd.read_csv("data/train_2016_v2.csv", parse_dates=['transactiondate'])
        train['month'] = train['transactiondate'].apply(lambda x: x.month)
        train_df = train.merge(prop, how='left', on='parcelid')
        #del prop
        gc.collect()

        self.train_df = train_df


    def create_sets(self):
        train_df = self.train_df
        x_train = train_df.query('month <=7 and abs(logerror) <=0.8')
        x_val =  train_df.query('month ==8')
        x_test = train_df.query('month ==9')
        self.x_train = x_train
        self.x_val = x_val
        self.x_test = x_test

    def create_sets_for_submission(self):
        train_df = self.train_df
        x_train = train_df.query('month <=11 and abs(logerror) <=2.0')
        x_val = train_df.query('month ==12')
        return x_train,x_val

    def add_month_dependent_feature(self, x_train=None, x_val=None, x_test=None):
        x_train = self.x_train if x_train is None else x_train
        x_val = self.x_val if x_val is None else x_val
        x_test = self.x_test if x_test is None else x_test

        mm = x_train.groupby(by=['regionidzip', 'month'])['logerror'] \
            .agg({
            #'regionidzip_count_logerror': np.count_nonzero
             'regionidzip_mean_logerror': np.mean
            #'regionidzip_median_logerror': np.median
            # ,'regionidzip_std_logerror': np.std
        })
        mm.fillna(100, inplace=True)

        mm_flat = mm.reset_index();
        mm_flat['month'] = mm_flat['month']+1
        mm = mm_flat.set_index(['regionidzip','month'])

        x_train = x_train.merge(mm_flat, how='left', on=['regionidzip', 'month'])
        x_train['regionidzip_mean_logerror'].fillna(0,inplace=True)

        #helper.use_linear_reg(mm, x_val, mm_stat_feature='regionidzip_count_logerror')
        #helper.use_linear_reg(mm, x_test, mm_stat_feature='regionidzip_count_logerror')
        helper.use_linear_reg(mm, x_val, mm_stat_feature='regionidzip_mean_logerror')
        helper.use_linear_reg(mm, x_test, mm_stat_feature='regionidzip_mean_logerror')
        x_val['regionidzip_mean_logerror'].fillna(0,inplace=True)
        x_test['regionidzip_mean_logerror'].fillna(0, inplace=True)

        self.x_train = x_train

    def select_features(self):
        x_train = self.x_train

        reload(preprocess)
        cat_features = preprocess.find_categorical_features(x_train)
        cols = x_train.columns
        cols_input = cols.drop(['parcelid', 'logerror', 'transactiondate', 'month'])

        self.cols_input = cols_input.tolist()
        self.cat_features = cat_features

    def train_catboost(self):
        cb = CatBoostRegressor(iterations=500, learning_rate=0.005, depth=5, l2_leaf_reg=5
                               , loss_function='MAE'
                               , eval_metric='MAE', od_type='Iter', verbose=True)

        cat_features_idx = [self.cols_input.index(c) for c in self.cat_features]
        print("Fitting Catboost",end="...")
        cb.fit(self.x_train[self.cols_input],self.x_train.logerror
               ,cat_features=cat_features_idx
               ,eval_set=(self.x_val[self.cols_input],self.x_val.logerror)
               )

        print("done")
        self.cb = cb


    def _train(self, x_train = None, x_val=None):

        # x_train = self.x_train
        # x_val = self.x_val
        # x_test = self.x_test
        # cols_input = self.cols_input

        x_train = self.x_train if x_train is None else x_train
        x_val = self.x_val if x_val is None else x_val

        cols_input = self.cols_input


        lgmr_woc = LGBMRegressor(n_estimators=3000, objective='mae', reg_alpha=5, max_bin=100 \
                                 ,reg_lambda=0
                                 ,min_child_samples=50, num_leaves=255\
                                 ,learning_rate=0.2
                                 ,subsample=0.5

                                 ,colsample_bytree=0.5
                                 )
        print("Fitting LGMR", end="...")
        lgmr_woc.fit(x_train[cols_input], x_train['logerror']
                     , sample_weight=x_train['month']
                     , eval_set=(x_val[cols_input], x_val['logerror']) \
                     , eval_sample_weight=[x_val['month']]
                     #,categorical_feature=self.cat_features
                     , eval_metric='mae', early_stopping_rounds=100
                     #,categorical_feature=self.cat_features
                     )
        print("done")
        #lr = LinearRegression()
        #lr.fit(x_train[cols_input], x_train['logerror'])

        return lgmr_woc

    def train_mean(self):

        x_train = self.x_train
        zip_mean = x_train.groupby(by=['regionidzip', 'month'])['logerror'].agg(np.mean).reset_index()


    def _train_linear(self,x_train=None):

        x_train = self.x_train if x_train is None else x_train
        cols_input = self.cols_input

        lr = LinearRegression()
        #0.0664051989931 with alpha = 0.2
        #0.0664081178199 with alpha = 0.5
        lr = Ridge(alpha=5,normalize=True)
        pf = PolynomialFeatures(interaction_only=True,degree=1)
        print("Fitting Linear model", end="...")
        f = pf.fit_transform(x_train[cols_input])
        self.pf = pf
        lr.fit(f, x_train['logerror'])
        self.lr = lr
        print("done")
        return lr

    def train(self, x_train=None, x_val=None):
        lgmr_woc = self._train(x_train,x_val)
        self.lgmr_woc = lgmr_woc
        gc.collect()

    def test(self):
        # Base: 0.06591372710093496 with abs(error) <2 and with feature2
        # 0.065946965740963884 with abs(error) <2 and with feature3
        # 0.065718084534645699 with abs(error) <2, with feature2 woc lgm
        # 0.065570623438523837 with abs(error) <2, with feature2 woc lgm w linear regr

        # 0.0656925737718
        # 0.0661480375025
        # 0.0656010717034

        try:
            self.cb
            utils.predict(self.cb, self.x_test, self.cols_input, 'p_cb')
            res = mean_absolute_error(self.x_test.logerror, self.x_test.p_cb)
            print("Cat boost:%s" % res)
        except AttributeError:
            pass

        utils.predict(self.lgmr_woc, self.x_test, self.cols_input, 'p_global_woc')
        res = mean_absolute_error(self.x_test.logerror, self.x_test.p_global_woc)
        print(res)

        lr = self._train_linear()
        #linear_score = lr.predict(self.x_test[self.cols_input])
        linear_score = lr.predict(self.pf.transform(self.x_test[self.cols_input]))
        print(mean_absolute_error(self.x_test.logerror,linear_score))
        mix = 0.7*self.x_test.p_global_woc + 0.3* linear_score
        res = mean_absolute_error(self.x_test.logerror,mix)
        print(res)

        mean = self.x_train.logerror.mean()
        mean_mix = 0.1*mean + 0.9*mix
        res = mean_absolute_error(self.x_test.logerror, mean_mix)
        print(res)

    def run(self,fv=5,lf=True,sf=False):

        ff = "data/pp_features%s_properties_2016.pickle"%(fv)

        if lf :
            self.prop = pickle.load(open(ff,"rb"))
        else:
            self.load_prop()
            self.create_features(fv)
            if sf:
                print("Saving to file %s" % (ff), end="...")
                pickle.dump(self.prop, open(ff, 'wb'))
                print("saved.")

        reload(alt_feature)
        alt_feature.add_feature(self.prop)
        alt_feature.remove_feature(self.prop)

        self.prepare_train_df()
        self.create_sets()
        #self.add_month_dependent_feature()
        self.select_features()
        #self.train_catboost()
        self.train()
        self._train_linear()
        self.test()

    def show_imp(self):
        cols_input = self.cols_input
        lgmr_woc = self.lgmr_woc
        imp = pd.DataFrame(data={'features': cols_input, 'importance': lgmr_woc.feature_importances_})
        print(imp.sort_values(by='importance'))

    def submit(self,fileno):
        prop = self.prop
        cols_input = self.cols_input

        x_train,x_val = self.create_sets_for_submission()

        print("Will train and make prediction using linear model")
        lr = self._train_linear(x_train)
        utils.predict_linear(lr, self.pf, prop, cols_input, 'p_linear', batch_size=100000)

        print("Will train and make prediction using LGM model")
        lgmr_woc = self._train(x_train,x_val)
        utils.predict(lgmr_woc, prop, cols_input, 'p_global_woc', batch_size=100000)


        score = 0.8*prop['p_global_woc'] + 0.2*prop['p_linear']

        sub = pd.DataFrame()
        sub['ParcelId'] = prop['parcelid']
        sub['201610'] = [float(format(x, '.4f')) for x in score]
        sub['201611'] = sub['201610']
        sub['201612'] = sub['201610']
        sub['201710'] = 0
        sub['201711'] = 0
        sub['201712'] = 0

        file_name ="ana/results%s.csv"%(fileno)
        sub.to_csv(file_name, index=False)

    def search_best_params(self):

        x_train = self.x_train
        x_val = self.x_val
        cols_input = self.cols_input

        params = {'n_estimators':[100,500,1000,1500]\
            ,'max_bin':[25,50,100,255]\
            ,'min_child_samples':[10,30,50,100]\
            ,'num_leaves':[64,255,512,1024]\
            ,'learning_rate':[0.001,0.05,0.1,0.5]\
            ,'objective':['mae']\
            , 'reg_alpha':[0.5,5,10,20,40]\
            ,'subsample':[0.6,0.7,0.8,1.0]
            }

        fit_params = {
            'eval_set': (x_val[cols_input], x_val['logerror'])\
             ,'eval_metric':'mae'\
            ,'early_stopping_rounds': 100\
            ,'sample_weight': x_train['month']\
        }
        gs = GridSearchCV(LGBMRegressor(),param_grid=params,scoring='neg_mean_absolute_error'
                          ,fit_params=fit_params
                          ,n_jobs=4
                          )
        gs.fit(x_train[cols_input],x_train['logerror'])

        return gs
pass

pd.options.display.width = 2000
