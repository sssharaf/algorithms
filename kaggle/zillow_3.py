
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.neighbors import LSHForest as LSH
from types import SimpleNamespace
import sklearn.preprocessing as pp
# New features
#   1. Number of similar houses nearby
#   2. Number of similar houses sold
#   3. Std of logerror of similar houses
#   4. Latitude & longitude

pd.options.display.width = 2000
pd.options.display.max_columns=100

def preprocess(props_df,cols_to_pp,encoder=None, scaler=None):
    props_df = props_df.copy()[cols_to_pp]
    if( encoder is None) :
        print("Encoding results with OneHotEncoder")
        category_features_idx = [i for i, dtype in enumerate(props_df.dtypes) if (pd.api.types.is_categorical_dtype(dtype))]
        print(category_features_idx)
        encoder = pp.OneHotEncoder(categorical_features=category_features_idx,sparse=False)
        encoder.fit(props_df )
    props_df_encoded = encoder.transform(props_df)
    if(scaler is None):
        print("Scaling results with RobustScaler")
        scaler = pp.RobustScaler(with_centering=False)
        scaler.fit(props_df_encoded)
    props_df_encoded_scaled= scaler.transform(props_df_encoded)
    r = SimpleNamespace()
    r.scaler = scaler
    r.encoder = encoder
    r.encoded_scaled_result = props_df_encoded_scaled
    return r


prop = pd.read_csv('data/properties_2016.csv')

train = pd.read_csv("data/train_2016_v2.csv",parse_dates=['transactiondate'])

train['month'] =  train['transactiondate'].apply(lambda x: x.month)

imp_features = pd.read_csv('ana/feature_importance.csv')

all_houses = prop.drop(labels = imp_features.query('importance >4')['features'], axis=1)

all_houses = all_houses.merge(train, how='outer', on='parcelid')

all_houses['sold'] = all_houses.apply(func=lambda x: 0 if np.isnan(x['logerror']) else 1,axis=1)


cols_train= all_houses.columns.drop(['parcelid', 'logerror', 'transactiondate', 'sold','month'])

x_train = all_houses.query('sold ==1').query('month<10')

lsh = LSH()
d,i = lsh.kneighbors(all_houses.iloc[0:1].drop(['parcelid'], axis=1))

for c in all_houses.columns:
    all_houses[c] = all_houses[c].fillna(-1)
    if all_houses[c].dtype == 'object':
        lbl = LabelEncoder()
        lbl.fit(list(all_houses[c].values))
        all_houses[c] = lbl.transform(list(all_houses[c].values))


train_df = train.merge(prop, how='left', on='parcelid')
cols = train_df.columns
cols = []
x_train = train_df[cols.drop(['parcelid', 'logerror', 'transactiondate'])]
y_train = train_df["logerror"].values.astype(np.float32)