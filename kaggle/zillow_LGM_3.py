# With Gradient boosting - giving best results yet

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features2 as features
from importlib import reload
import gc
from types import SimpleNamespace
import kaggle.LGM_2 as helper
import kaggle.utils as utils
import pickle
from sklearn.linear_model import Ridge,LinearRegression
from sklearn.preprocessing import PolynomialFeatures

gc.collect()

pd.options.display.width = 2000

# prop = pd.read_csv('data/properties_2016.csv')
# preprocess.preprocess(prop,lat_long_pp=True)
# pickle.dump(prop,open('data/pp_ll_properties_2016.pickle','wb'))

prop= pickle.load(open('data/pp_properties_2016.pickle','rb'))


reload(features)
features.create_features(prop)
pickle.dump(prop,open('data/pp_features_properties_2016.pickle','wb'))
gc.collect()

#prop= pickle.load(open('data/pp_features_properties_2016.pickle','rb'))
#prop= pickle.load(open('data/pp_features2_properties_2016.pickle','rb'))


train = pd.read_csv("data/train_2016_v2.csv",parse_dates=['transactiondate'])
train['month'] =  train['transactiondate'].apply(lambda x: x.month)
train_df = train.merge(prop, how='left', on='parcelid')
del prop
gc.collect()


# x_train = train_df.query('month <=9 and abs(logerror) <=2.0')
# x_val = train_df.query('month ==10')
# x_test = train_df.query('month >10')
x_train = train_df.query('transactiondate < "10-1-2016" and abs(logerror) <=2.0')
x_val = train_df.query('transactiondate >= "10-1-2016" and transactiondate <= "10-15-2016"')
x_test = train_df.query('transactiondate > "10-15-2016"')


mm = x_train.append(x_val).groupby(by=['regionidzip','month'])['logerror']  \
    .agg({
                'regionidzip_count_logerror':np.count_nonzero
                  ,'regionidzip_mean_logerror': np.mean
                  #,'regionidzip_median_logerror': np.median
                 # ,'regionidzip_std_logerror': np.std
          })
mm.fillna(100,inplace=True)
#mm.reset_index(inplace=True)

pf = PolynomialFeatures()
mm_features = pf.fit_transform(mm.loc[:,['month','regionidzip']])

mm_flat = mm.reset_index();
x_train=x_train.merge(mm_flat,how='left',on=['regionidzip','month'])
x_val=x_val.merge(mm_flat,how='left',on=['regionidzip','month'])

gc.collect()

reload(preprocess)
cat_features = preprocess.find_categorical_features(train_df)
cols = x_train.columns
cols_input = cols.drop(['parcelid', 'logerror', 'transactiondate','month'])

helper.use_linear_reg(mm,x_test,mm_stat_feature='regionidzip_count_logerror')
helper.use_linear_reg(mm,x_test,mm_stat_feature='regionidzip_mean_logerror')
helper.use_linear_reg(mm,x_test,mm_stat_feature='regionidzip_median_logerror')
helper.use_linear_reg(mm,x_test,mm_stat_feature='regionidzip_std_logerror')


reload(helper)
#zip_dic = helper.create_est2('regionidzip',x_train,x_val,x_test,cols_input,cat_features)
#city_dic = helper.create_est2('regionidcity',x_train,x_val,x_test,cols_input,cat_features)

gc.collect()

lgmr_woc = LGBMRegressor(n_estimators=3000,objective='mae', reg_alpha=20,max_bin=512\
                     ,min_child_samples=50,num_leaves=63,learning_rate=0.2)

lgmr_woc.fit(x_train[cols_input], x_train['logerror'],eval_set=(x_val[cols_input],x_val['logerror'])\
         ,eval_metric='mae',early_stopping_rounds=100, sample_weight=x_train['month'])

# Overfit but works
lgmr_woc = LGBMRegressor(n_estimators=1000,objective='mae', reg_alpha=20,max_bin=100\
                     ,min_child_samples=50,num_leaves=512,learning_rate=0.5)

lgmr_woc.fit(x_train[cols_input], x_train['logerror'],eval_set=(x_val[cols_input],x_val['logerror'])\
         ,eval_metric='mae',early_stopping_rounds=100, sample_weight=x_train['month'])

gc.collect()

imp = pd.DataFrame(data={'features':cols_input,'importance':lgmr_woc.feature_importances_})
imp.sort_values(by='importance')



utils.predict(lgmr_woc,x_test,cols_input,'p_global_woc')
# 0.068967444921983478 with features & mean,count of logerror at zip level
# 0.068595223570915936 with features
mean_absolute_error(x_test.logerror,x_test.p_global_woc)



lgmr = LGBMRegressor(n_estimators=3000,objective='mae', reg_alpha=2,max_bin=512\
                     ,min_child_samples=50,num_leaves=31)

lgmr.fit(x_train[cols_input], x_train['logerror'],categorical_feature=cat_features,eval_set=(x_val[cols_input],x_val['logerror'])\
         ,eval_metric='mae',early_stopping_rounds=100, sample_weight=x_train['month'])

gc.collect()
imp = pd.DataFrame(data={'features':cols_input,'importance':lgmr.feature_importances_})
imp.sort_values(by='importance')
utils.predict(lgmr,x_test,cols_input,'p_global')

mean_absolute_error(x_test.logerror,x_test.p_global)

# 0.074362607901854033
# 0.074356056959540326
mean_absolute_error(x_test.logerror,0.7*x_test.p_global_woc + 0.3*x_test.p_global)


#helper.predict(x_test,zip_dic,'regionidzip',cols_input)
#helper.predict(x_test,city_dic,'regionidcity',cols_input)
gc.collect()

prop.p_global.fillna(0,inplace=True)
prop.p_regionidzip.fillna(prop.p_global,inplace=True)
prop.p_regionidcity.fillna(prop.p_global,inplace=True)


#0.068731898068767602
#mean_absolute_error(x_test.logerror,x_test.p_regionidzip)
#0.068367523124861787
#mean_absolute_error(x_test.logerror,x_test.p_regionidcity)

#0.07480232494579743 with x_test ==12
#0.06832999047435187
#mean_absolute_error(x_test.logerror,x_test.p_global_woc)
#0.068347084915558215
# mean_absolute_error(x_test.logerror,\
#                     .5*x_test.p_global +\
#                     .5*x_test.p_regionidcity\
#                     )

#0.068352563174374367
# mean_absolute_error(x_test.logerror,\
#                     .6*x_test.p_global +\
#                     .1*x_test.p_regionidzip +\
#                     .3*x_test.p_regionidcity\
#                     )

# prop['result'] = .3*prop['p_global'] + .4*prop['p_regionidzip']+.3*prop['p_regionidcity']

# mean_absolute_error(x_test.logerror,\
#                     .6*x_test.p_global +\
#                     .1*x_test.p_regionidzip +\
#                     .3*x_test.p_regionidcity\
#                     )



#Base result: 0.065555229888043298 - with abs(logerror) <=0.3
#0.065466586240686742


# params = {'n_estimators':[30,50,60,120,150,200,500]\
#     ,'max_bin':[255,512,1024,2048,4096,8192,16000,25000]\
#     ,'min_child_samples':[50,70,100,150,200]\
#     ,'num_leaves':[7,15,31,41,61]\
#     ,'learning_rate':[0.001,0.005,0.01,0.05,0.1,0.5]\
#     ,'objective':['mse','mae','huber','fair']\
#     , 'reg_alpha':[0.001,0.005,0.01,0.05,0.5,1,2,4]\
#     ,'reg_lambda':[0.001,0.005,0.01,0.05,0.5,1,2,4]\
#         }
#
# gs = GridSearchCV(LGBMRegressor(),params,scoring='neg_mean_absolute_error')
# gs.fit(x_train[cols_input],y_train)

# lgmr = LGBMRegressor(n_estimators=100,objective='mae', reg_alpha=20,max_bin=500\
#                      ,min_child_samples=100,num_leaves=31\
#                      ,reg_lambda=0,subsample_for_bin=10000)

#For m ==12
# lgmr = LGBMRegressor(n_estimators=300,objective='mae', reg_alpha=5,max_bin=500\
#                      ,min_child_samples=10,num_leaves=31,reg_lambda=0,subsample_for_bin=100000)

#Submission

gc.collect()
#prop = pd.read_csv('data/pp_properties_2016.csv')
prop= pickle.load(open('data/pp_properties_2016.pickle','rb'))
prop.fillna(0,inplace=True)
gc.collect()
reload(features)
features.create_features(prop)
gc.collect()

prop['month']=10
helper.use_linear_reg(mm,prop,mm_stat_feature='regionidzip_count_logerror')
helper.use_linear_reg(mm,prop,mm_stat_feature='regionidzip_mean_logerror')
utils.predict(lgmr_woc,prop,cols_input,'p_global_woc_10',batch_size=100000)

prop['month']=11
helper.use_linear_reg(mm,prop,mm_stat_feature='regionidzip_count_logerror')
helper.use_linear_reg(mm,prop,mm_stat_feature='regionidzip_mean_logerror')
utils.predict(lgmr_woc,prop,cols_input,'p_global_woc_11',batch_size=100000)

prop['month']=12
helper.use_linear_reg(mm,prop,mm_stat_feature='regionidzip_count_logerror')
helper.use_linear_reg(mm,prop,mm_stat_feature='regionidzip_mean_logerror')
utils.predict(lgmr_woc,prop,cols_input,'p_global_woc_12',batch_size=100000)


score = prop['p_global_woc']
sub=pd.DataFrame()
sub['ParcelId'] = prop['parcelid']
sub['201610'] = [float(format(x, '.4f')) for x in prop['p_global_woc_10']]
sub['201611'] = [float(format(x, '.4f')) for x in prop['p_global_woc_11']]
sub['201612'] = [float(format(x, '.4f')) for x in prop['p_global_woc_12']]
sub['201710'] = 0
sub['201711'] = 0
sub['201712'] = 0

sub.to_csv('ana/results32.csv',index=False)