
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.neighbors import LSHForest as LSH
from types import SimpleNamespace
import sklearn.preprocessing as pp

def encode(props_df,encoder=None, scaler=None):
    props_df = props_df.copy()
    if( encoder is None):
        print("Encoding results with OneHotEncoder")
        category_features_idx = [i for i, dtype in enumerate(props_df.dtypes) if (pd.api.types.is_categorical_dtype(dtype))]
        print("Categorical features:{0}".format(category_features_idx))
        encoder = pp.OneHotEncoder(categorical_features=category_features_idx,sparse=False)
    encoder.fit(props_df)
    props_df_encoded = encoder.transform(props_df)
    ns = SimpleNamespace()
    return props_df_encoded

