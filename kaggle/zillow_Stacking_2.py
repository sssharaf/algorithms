# With Gradient boosting - giving best results yet

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features2 as features2
import kaggle.features3 as features3
import kaggle.features4 as features4
import kaggle.features5 as features5
import kaggle.features6 as features6
from importlib import reload
import gc
from types import SimpleNamespace
import kaggle.LGM_2 as helper
import kaggle.utils as utils
import pickle
from sklearn.linear_model import Ridge,LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import kaggle.alterable_features as alt_feature
from catboost import CatBoostRegressor
from mlxtend.regressor import StackingRegressor
from sklearn.externals import joblib
from mlens.ensemble import Subsemble,BlendEnsemble
from sklearn.metrics.scorer import neg_mean_absolute_error_scorer
import kaggle.partitioners as partitioners
import kaggle.MyLgbm as mylgm
from mlens.model_selection import Evaluator
from mlens.metrics import make_scorer
from mlens.preprocessing import  Subset
from scipy.stats import uniform,randint
from sklearn.neighbors import KNeighborsRegressor as KNR
from sklearn.cluster import KMeans
from sklearn.pipeline import make_pipeline
from sklearn.linear_model import Ridge,Lasso
from sklearn.preprocessing import PolynomialFeatures
import kaggle.MyCatBoost as MyCatBoost
pd.options.display.width = 2000
pd.options.display.max_columns=100

class Stack_2(object):

    def load_prop(self):
        prop = pickle.load(open('data/pp_woc_properties_2016.pickle', 'rb'))
        prop.fillna(0, inplace=True)
        self.prop = prop

    def create_features(self,version=3):
        prop = self.prop
        if version ==2:
            reload(features2)
            features2.create_features(prop)
        elif version ==3:
            reload(features3)
            features3.create_features(prop)
        elif version == 4:
            reload(features4)
            features4.create_features(prop)
        elif version == 5:
            reload(features5)
            features5.create_features(prop)
        else :
            raise BaseException("Not supported")

    def prepare_train_df(self,year):

        trnfile = None
        if year == 2016:
            trnfile = "data/train_2016_v2.csv"
        elif year ==2017:
            trnfile = "data/train_2017.csv"
        print("Reading training data from :%s"%trnfile)
        prop = self.prop
        train = pd.read_csv(trnfile, parse_dates=['transactiondate'])
        train['month'] = train['transactiondate'].apply(lambda x: x.month)
        if year==2017:
            train2016 = pd.read_csv("data/train_2016_v2.csv", parse_dates=['transactiondate'])
            train2016['month'] = train2016['transactiondate'].apply(lambda x: x.month)
            rest2016 = train2016.loc[train2016.month >= 10]
            train = train.append(rest2016)

        train_df = train.merge(prop, how='left', on='parcelid')
        train_df.loc[train_df.regionidcounty==0,'regionidcounty'] = train_df.regionidcounty.mode().values[0]
        #del prop
        gc.collect()

        self.train_df = train_df


    def create_sets(self,year):
        train_df = self.train_df
        if year ==2016:
            x_train = train_df.query('month <=12 and abs(logerror) <=0.8')
            x_val =  train_df.query('month ==11')
            x_test = train_df.loc[train_df.month ==12]
        elif year ==2017:
            x_train = train_df.query('month <=12 and abs(logerror) <=0.8')
            x_val =  train_df.query('month ==11')
            x_test = train_df.loc[train_df.month==train_df.month.max()]

        self.x_train = x_train
        self.x_val = x_val
        self.x_test = x_test

    def create_sets_for_submission(self):
        train_df = self.train_df
        x_train = train_df.query('month <=11 and abs(logerror) <=2.0')
        x_val = train_df.query('month ==12')
        return x_train,x_val

    def add_month_dependent_feature(self, x_train=None, x_val=None, x_test=None):
        x_train = self.x_train if x_train is None else x_train
        x_val = self.x_val if x_val is None else x_val
        x_test = self.x_test if x_test is None else x_test

        mm = x_train.groupby(by=['regionidzip', 'month'])['logerror'] \
            .agg({
            #'regionidzip_count_logerror': np.count_nonzero
             'regionidzip_mean_logerror': np.mean
            #'regionidzip_median_logerror': np.median
            # ,'regionidzip_std_logerror': np.std
        })
        mm.fillna(100, inplace=True)

        mm_flat = mm.reset_index();
        mm_flat['month'] = mm_flat['month']+1
        mm = mm_flat.set_index(['regionidzip','month'])

        x_train = x_train.merge(mm_flat, how='left', on=['regionidzip', 'month'])
        x_train['regionidzip_mean_logerror'].fillna(0,inplace=True)

        #helper.use_linear_reg(mm, x_val, mm_stat_feature='regionidzip_count_logerror')
        #helper.use_linear_reg(mm, x_test, mm_stat_feature='regionidzip_count_logerror')
        helper.use_linear_reg(mm, x_val, mm_stat_feature='regionidzip_mean_logerror')
        helper.use_linear_reg(mm, x_test, mm_stat_feature='regionidzip_mean_logerror')
        x_val['regionidzip_mean_logerror'].fillna(0,inplace=True)
        x_test['regionidzip_mean_logerror'].fillna(0, inplace=True)

        self.x_train = x_train

    def select_features(self,x_train=None,set_to_self=True):
        x_train = self.x_train if x_train is None else x_train

        reload(preprocess)
        cat_features = preprocess.find_categorical_features(x_train)
        cols = x_train.columns
        cols_input = cols.drop(['parcelid', 'logerror', 'transactiondate'])

        cols_input = cols_input.tolist()
        cat_features = cat_features

        if(set_to_self):
            self.cols_input = cols_input
            self.cat_features = cat_features
        return(cols_input,cat_features)

    def tr(self,x):
            x = abs(x)
            if x <= 0.05:
                return 10
            elif x <= 0.1:
                return 8
            elif x <=0.2 :
                return 6
            elif x <= 0.4:
                return 3
            return 1

    def test(self):
        # Base: 0.06591372710093496 with abs(error) <2 and with feature2
        # 0.065946965740963884 with abs(error) <2 and with feature3
        # 0.065718084534645699 with abs(error) <2, with feature2 woc lgm
        # 0.065570623438523837 with abs(error) <2, with feature2 woc lgm w linear regr

        # 0.0656925737718
        # 0.0661480375025
        # 0.0656010717034

        utils.predict(self.cb, self.x_test, self.cols_input, 'p_cb')
        res = mean_absolute_error(self.x_test.logerror, self.x_test.p_cb)
        print("Cat boost:%s" % res)

        utils.predict(self.lgbr, self.x_test, self.cols_input, 'p_global_woc')
        res = mean_absolute_error(self.x_test.logerror, self.x_test.p_global_woc)
        print("LGBR boost:%s" % res)

        utils.predict_linear(self.lr,self.pf, self.x_test, self.cols_input, 'p_lr')
        res =mean_absolute_error(self.x_test.logerror,self.x_test.p_lr)
        print("Linear boost:%s" % res)

        mix = 0.5*self.x_test.p_global_woc + 0.3*self.x_test.p_cb + 0.1* self.x_test.p_lr
        res = mean_absolute_error(self.x_test.logerror,mix)
        print("Mix boost:%s" % res)


        mean = self.x_train.logerror.mean()
        mean_mix = 0.1*mean + 0.9*mix
        res = mean_absolute_error(self.x_test.logerror, mean_mix)
        print(res)

    def test_super(self,x_test =None):

        x_test = self.x_test if x_test is None else x_test

        cols_input, cat_features = self.select_features(self.x_train, set_to_self=False)
        cols_input.remove('fold')
        utils.predict(self.super_estimator, x_test, cols_input, 'p_super')

    def load_props_and_features(self,year,fv=6,lf=True,sf=False):
        ff = "data/pp_features%s_properties_%s.pickle" % (fv,year)
        if lf:
            self.prop = pickle.load(open(ff, "rb"))
        else:
            self.load_prop()
            self.create_features(fv)
            if sf:
                print("Saving to file %s" % (ff), end="...")
                pickle.dump(self.prop, open(ff, 'wb'))
                print("saved.")


    def create_ensemble(self):

        est_1_1 = LGBMRegressor(n_estimators=50, objective='mae', reg_alpha=5, max_bin=100 \
                                , reg_lambda=20
                                , min_child_samples=50, num_leaves=255 \
                                , learning_rate=0.03
                                , subsample=0.8
                                , subsample_for_bin=90000
                                , colsample_bytree=0.8)
        est_1_1.fit(self.x_train[self.cols_input],self.x_train.logerror)
        imp_features = self.show_imp(est_1_1)

        more_imp_features= imp_features[-40:]
        print(more_imp_features)

        neg_mae = lambda y, y_pred: mean_absolute_error(y, y_pred)
        ens = Subsemble(scorer=neg_mae, verbose=True, partitions=1, folds=2,array_check=2)

        est_1_1 = LGBMRegressor(n_estimators=50, objective='mae', reg_alpha=5, max_bin=100 \
                                , reg_lambda=15
                                , min_child_samples=50, num_leaves=255 \
                                , learning_rate=0.02
                                , subsample=0.6
                                ,subsample_for_bin=90000
                                , colsample_bytree=0.4,seed=1)

        regionidcounty_idx = utils.find_col_index(self.cols_input, 'regionidcounty')
        structuretaxvaluedollarcnt_idx = utils.find_col_index(self.cols_input, 'structuretaxvaluedollarcnt')
        calculatedfinishedsquarefeet_idx = utils.find_col_index(self.cols_input, 'calculatedfinishedsquarefeet')
        yearbuilt_idx = utils.find_col_index(self.cols_input, 'yearbuilt')

        estimators_1 = {
            "case-1": [("est1", est_1_1)]
            #,"case-2": [("est2", est_1_1)]
            ,"linear-model":[Lasso(normalize=True,alpha=30,max_iter=3000)]
        }
        pp_1 = {
            "case-1": []
            #,"case-2": [Subset(utils.find_col_index(self.cols_input,more_imp_features))]
            , "linear-model": [PolynomialFeatures(interaction_only=True,degree=1)]
        }
        # 0.0739910931033
        ens.add(estimators=estimators_1, preprocessing=pp_1
                #, partitions=3
                #, partition_estimator=make_pipeline(Subset([yearbuilt_idx])
                #                                    , KMeans(3))
                # ,partition_estimator=partitioners.ColumnPartitioner(utils.find_col_index(self.cols_input,'buildingqualitytypeid'))
                , propagate_features=utils.find_col_index(self.cols_input, self.cols_input)
                )

        estimators_2 = estimators_1
        pp_2=pp_1
        #0.0741312526195 with layer -2 commented
        # ens.add(estimators=estimators_2, preprocessing=pp_2
        #         , partitions=3
        #         , partition_estimator=partitioners.ColumnPartitioner(regionidcounty_idx)
        #         , propagate_features=utils.find_col_index(self.cols_input, self.cols_input)
        #         )

        ridge = Ridge(normalize=True, alpha=15)
        lasso = Lasso(normalize=True, alpha=31)

        estimators_3 = {
             "case-1": [('ridge',ridge),('lasso',lasso)]
        }
        pp_3 = {
            "case-1": [ PolynomialFeatures(interaction_only=True,degree=1)]
        }
        # ens.add(estimators=estimators_3, preprocessing=pp_3
        #         , propagate_features=utils.find_col_index(self.cols_input, self.cols_input)
        #         )

        meta =    LGBMRegressor(n_estimators=100, objective='mae', reg_alpha=5, max_bin=100 \
                                , reg_lambda=15
                                , min_child_samples=50, num_leaves=100 \
                                , learning_rate=0.02
                                , subsample=0.8
                                ,subsample_for_bin=90000
                                , colsample_bytree=0.6)

        meta = meta

        ens.add_meta(meta)
        ens.fit(self.x_train[self.cols_input], self.x_train.logerror)
        p = ens.predict(self.x_test[self.cols_input])
        print(mean_absolute_error(self.x_test.logerror, p))
        self.ens = ens
        return ens

    def run_w_stack(self, year,fv=6, lf=True, sf=False, lm=True):
        self.year = year
        self.load_props_and_features(year,fv,lf,sf)
        reload(alt_feature)
        alt_feature.add_feature(self.prop)
        alt_feature.remove_feature(self.prop)

        self.prepare_train_df(year)
        self.create_sets(year)
        self.select_features()


    def show_imp(self,estimator):
        cols_input = self.cols_input
        imp = pd.DataFrame(data={'features': cols_input, 'importance': estimator.feature_importances_})
        return list(imp.sort_values(by='importance').reset_index().features.values)

    def submit(self,fileno):
        year = self.year
        prop = self.prop
        prop['month']=10
        utils.predict(self.ens,prop,self.cols_input,'p_super',3000000)

        sub = pd.DataFrame()

        sub['ParcelId'] = prop['parcelid']
        if(year == 2016):
            sub['201610'] = [float(format(x, '.4f')) for x in prop.p_super]
            sub['201611'] = sub['201610']
            sub['201612'] = sub['201610']
        elif year == 2017:
            sub['201710'] = [float(format(x, '.4f')) for x in prop.p_super]
            sub['201711'] = sub['201710']
            sub['201712'] = sub['201710']

        file_name ="ana/results%s_%s.csv"%(fileno,year)
        print("Writing results to file :%s"%file_name)
        sub.to_csv(file_name, index=False)

    def search_best_params(self):

        x_train = self.x_train
        x_val = self.x_val
        cols_input = self.cols_input

        params = {
            'lgbmregressor': {
                'learning_rate': uniform(0.02, 0.1)
                , 'num_leaves': randint(100, 512)
                , 'n_estimators': randint(50, 700)
            }
        }

        fit_params = {
            'eval_set': (x_val[cols_input], x_val['logerror'])\
             ,'eval_metric':'mae'\
            ,'early_stopping_rounds': 100\
            ,'sample_weight': x_train['month']\
        }
        gs = GridSearchCV(LGBMRegressor(),param_grid=params,scoring='neg_mean_absolute_error'
                          ,fit_params=fit_params
                          ,n_jobs=4
                          )
        gs.fit(x_train[cols_input],x_train['logerror'])

        return gs
pass

pd.options.display.width = 2000
