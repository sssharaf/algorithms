# With Gradient boosting - giving best results yet

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features2 as features2
import kaggle.features3 as features3
import kaggle.features4 as features4
import kaggle.features5 as features5
from importlib import reload
import gc
from types import SimpleNamespace
import kaggle.LGM_2 as helper
import kaggle.utils as utils
import pickle
from sklearn.linear_model import Ridge,LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import kaggle.alterable_features as alt_feature
from catboost import CatBoostRegressor
from mlxtend.regressor import StackingRegressor
from sklearn.externals import joblib
from mlens.ensemble import Subsemble,BlendEnsemble
from sklearn.metrics.scorer import neg_mean_absolute_error_scorer
import kaggle.partitioners as partitioners
import kaggle.MyLgbm as mylgm
from mlens.model_selection import Evaluator
from mlens.metrics import make_scorer
from mlens.preprocessing import  Subset
from scipy.stats import uniform,randint
from sklearn.neighbors import KNeighborsRegressor as KNR
from sklearn.cluster import KMeans
from sklearn.pipeline import make_pipeline
pd.options.display.width = 2000
pd.options.display.max_columns=100


prop_2016 = pd.read_csv('data/properties_2016.csv')
prop_2017 = pd.read_csv('data/properties_2017.csv')

prop_2017.drop(labels=['taxamount','taxvaluedollarcnt','structuretaxvaluedollarcnt'],axis=1,inplace=True)

prop_2017['taxamount']= prop_2016['taxamount']
prop_2017['taxvaluedollarcnt']= prop_2016['taxvaluedollarcnt']
prop_2017['structuretaxvaluedollarcnt']= prop_2016['structuretaxvaluedollarcnt']

prop_2017.to_csv('data/properties_updated_2016.csv',index=False)

preprocess.preprocess(prop_2017)

pickle.dump(prop_2017,open('data/pp_properties_updated_2016.pickle','wb'))


