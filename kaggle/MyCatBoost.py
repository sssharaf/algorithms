# With Gradient boosting - giving best results yet

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features2 as features2
import kaggle.features3 as features3
import kaggle.features4 as features4
import kaggle.features5 as features5
from importlib import reload
import gc
from types import SimpleNamespace
import kaggle.LGM_2 as helper
import kaggle.utils as utils
import pickle
from sklearn.linear_model import Ridge,LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import kaggle.alterable_features as alt_feature
from catboost import CatBoostRegressor
from mlxtend.regressor import StackingRegressor
from sklearn.externals import joblib
from mlens.ensemble import Subsemble
import traceback

pd.options.display.width = 2000
pd.options.display.max_columns=100

class MyCatBoostR(CatBoostRegressor):

    def __new__(cls,**kwds):
        return super().__new__(cls)

    def __init__(self, iterations=500, learning_rate=0.03, depth=6, l2_leaf_reg=3, rsm=1, loss_function='RMSE',
                 border=None, border_count=None, feature_border_type='MinEntropy', fold_permutation_block_size=None,
                 od_pval=None, od_wait=None, od_type=None, nan_mode=None, counter_calc_method=None,
                 gradient_iterations=None, leaf_estimation_method=None, thread_count=None, random_seed=None,
                 use_best_model=False, verbose=False, ctr_description=None, ctr_border_count=None,
                 ctr_leaf_count_limit=None, store_all_simple_ctr=False, max_ctr_complexity=None, priors=None,
                 has_time=False, one_hot_max_size=None, random_strength=1, name='experiment', ignored_features=None,
                 train_dir=None, custom_loss=None, eval_metric=None, bagging_temperature=None, save_snapshot=None,
                 snapshot_file=None, fold_len_multiplier=None, used_ram_limit=None, feature_priors=None,
                 allow_writing_files=None, approx_on_full_history=None, **kwargs):

        fit_params_calculator = None
        if hasattr(self,'fit_params_calculator'):
            fit_params_calculator = params['fit_params_calculator']
            delattr(self,'fit_params_calculator')

        super().__init__(iterations, learning_rate, depth, l2_leaf_reg, rsm, loss_function, border, border_count,
                         feature_border_type, fold_permutation_block_size, od_pval, od_wait, od_type, nan_mode,
                         counter_calc_method, gradient_iterations, leaf_estimation_method, thread_count, random_seed,
                         use_best_model, verbose, ctr_description, ctr_border_count, ctr_leaf_count_limit,
                         store_all_simple_ctr, max_ctr_complexity, priors, has_time, one_hot_max_size, random_strength,
                         name, ignored_features, train_dir, custom_loss, eval_metric, bagging_temperature,
                         save_snapshot, snapshot_file, fold_len_multiplier, used_ram_limit, feature_priors,
                         allow_writing_files, approx_on_full_history, **kwargs)

        if fit_params_calculator is not None:
            self.set_params(fit_params_calculator=fit_params_calculator)

    def set_fit_params_calc_func(self,func):
        self.set_params(fit_params_calculator=func)

    def fit(self,X,y,**kwds):
        params = self.get_params(deep=False)

        if hasattr(self,'fit_params_calculator'):
            fit_params_calculator = params['fit_params_calculator']
            delattr(self,'fit_params_calculator')

            fit_params=fit_params_calculator(X)
            r = super().fit(X,y,**fit_params)
            self.set_params(**fit_params)
            return r
        else:
            super().fit(X,y)

