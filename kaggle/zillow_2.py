# With knn

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
import sklearn.preprocessing as pp
import scipy
import scipy.spatial.ckdtree
from sklearn.metrics import mean_absolute_error

pd.options.display.width = 1000

prp = pd.read_csv('data/properties_2016.csv',memory_map=True)

trn = pd.read_csv("data/train_2016_v2.csv", parse_dates=['transactiondate'])

trn['month'] =  trn['transactiondate'].apply(lambda x: x.month)

m = prp.merge(trn,on='parcelid')

def gen_knn_estimator(data,cols):
    df_nn = data[cols].copy()
    df_nn.fillna(0,inplace=True)
    scaler = pp.RobustScaler()
    dd_nn_scaled = scaler.fit_transform(df_nn)
    knn = scipy.spatial.cKDTree(dd_nn_scaled)
    return knn,scaler

def calc_avg_logerror(r,cols,knn,scaler,k,y):
    r1 = r[cols]
    r1.fillna(0, inplace=True)
    r2 = scaler.transform(r1)
    d, i = knn.query(r2, k)
    err = y.iloc[i.reshape(-1)].reshape(-1,k)
    err = np.median(err, axis=1)
    return err

def calc_avg_dist(r,cols,knn,scaler,k):
    r1 = r[cols]
    r1.fillna(0, inplace=True)
    r2 = scaler.transform(r1)
    d, i = knn.query(r2, k)
    #err = y.iloc[i.reshape(-1)].reshape(-1,k)
    err = np.median(d, axis=1)
    return err

#coll_for_nn = ['bedroomcnt','bathroomcnt','latitude','longitude','structuretaxvaluedollarcnt','landtaxvaluedollarcnt']
coll_for_nn = ['latitude','longitude'
                ,'lotsizesquarefeet','taxamount'
               ]

nine_month = m.query('month <=9')
nine_month = nine_month.query('abs(logerror) <=0.3')
knn,scaler = gen_knn_estimator(nine_month,coll_for_nn)

test= m.query('month>9')

test['pred_1'] = calc_avg_logerror(test,coll_for_nn,knn,scaler,50,nine_month['logerror'])

data= m.copy()
knn,scaler = gen_knn_estimator(data,coll_for_nn)
prp['201610']=calc_avg_logerror(prp,coll_for_nn,knn,scaler,50,data['logerror'])
