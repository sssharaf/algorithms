# With Gradient boosting - giving best results yet

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features2 as features
from importlib import reload
import gc
from types import SimpleNamespace
import kaggle.LGM_2 as helper
import kaggle.utils as utils
import pickle
gc.collect()

pd.options.display.width = 2000

#prop = pd.read_csv('data/pp_properties_2016.csv')
prop= pickle.load(open('data/pp_properties_2016.pickle','rb'))
prop.fillna(0,inplace=True)

reload(features)
features.create_features(prop)
pickle.dump(prop,open('data/pp_features2_properties_2016.pickle','wb'))
#prop= pickle.load(open('data/pp_features_properties_2016.pickle','rb'))


train = pd.read_csv("data/train_2016_v2.csv",parse_dates=['transactiondate'])
train['month'] =  train['transactiondate'].apply(lambda x: x.month)
train_df = train.merge(prop, how='left', on='parcelid')
#del prop
gc.collect()

# a = train_df.groupby(by=['regionidzip','month'])['structuretaxvaluedollarcnt'].agg([np.count_nonzero,np.std])
# a.reset_index(inplace=True)
# a['month'] = a['month']+1
# train_df=train_df.merge(a,on=['regionidzip','month'])

reload(preprocess)
cat_features = preprocess.find_categorical_features(train_df)
cols = train_df.columns
cols_input = cols.drop(['parcelid', 'logerror', 'transactiondate','month'])



# x_train = train_df.query('month <=9 and abs(logerror) <=2.0')
# x_val = train_df.query('month ==10')
# x_test = train_df.query('month >10')
x_train = train_df.query('month <=10 and abs(logerror) <=2.0')
x_val = train_df.query('month ==11')
x_test = train_df.query('month >11')


gc.collect()


reload(helper)
#zip_dic = helper.create_est2('regionidzip',x_train,x_val,x_test,cols_input,cat_features)
#city_dic = helper.create_est2('regionidcity',x_train,x_val,x_test,cols_input,cat_features)

gc.collect()

lgmr_woc = LGBMRegressor(n_estimators=3000,objective='mae', reg_alpha=20,max_bin=512\
                     ,min_child_samples=50,num_leaves=63,learning_rate=0.2)

lgmr_woc.fit(x_train[cols_input], x_train['logerror'],eval_set=(x_val[cols_input],x_val['logerror'])\
         ,eval_metric='mae',early_stopping_rounds=100, sample_weight=x_train['month'])

# Overfit but works
lgmr_woc = LGBMRegressor(n_estimators=1000,objective='mae', reg_alpha=20,max_bin=100\
                     ,min_child_samples=50,num_leaves=512,learning_rate=0.2)

lgmr_woc.fit(x_train[cols_input], x_train['logerror'],eval_set=(x_val[cols_input],x_val['logerror'])\
         ,eval_metric='mae',early_stopping_rounds=100, sample_weight=x_train['month'])

gc.collect()

imp = pd.DataFrame(data={'features':cols_input,'importance':lgmr_woc.feature_importances_})
imp.sort_values(by='importance')

utils.predict(lgmr_woc,x_test,cols_input,'p_global_woc')
# 0.074442606835374264 with month <=10 xx_test==12 and log(error) < =0.8
# 0.074360841987436199 with month <=10 xx_test==12 and log(error) < =2 without create_ffeatures
#0.074403635054857642 with month <=10 xx_test==12 and log(error) < =2 with features
# 0.074327949064217427 with month <=10 xx_test==12 and log(error) < =2 with one feaure - structuretax_to_calculatedfinishedsquarefeet
#0.074376651797469565 with month <=10 xx_test==12 and log(error) < =2 and new fearure taxamount_kde
# 0.074106599409280369 with month <=10 xx_test==12 and log(error) < =3 and with features
# 0.067887570703220768 with month <=9 xx_test>10 and log(error) < =2 and no features
# 0.067919499626726235 with month <=9 xx_test>10 and log(error) < =2 and with features
mean_absolute_error(x_test.logerror,x_test.p_global_woc)



lgmr = LGBMRegressor(n_estimators=3000,objective='mae', reg_alpha=2,max_bin=512\
                     ,min_child_samples=50,num_leaves=31)

lgmr.fit(x_train[cols_input], x_train['logerror'],categorical_feature=cat_features,eval_set=(x_val[cols_input],x_val['logerror'])\
         ,eval_metric='mae',early_stopping_rounds=100, sample_weight=x_train['month'])

gc.collect()
imp = pd.DataFrame(data={'features':cols_input,'importance':lgmr.feature_importances_})
imp.sort_values(by='importance')
utils.predict(lgmr,x_test,cols_input,'p_global')

mean_absolute_error(x_test.logerror,x_test.p_global)

# 0.074362607901854033
# 0.074356056959540326
mean_absolute_error(x_test.logerror,0.7*x_test.p_global_woc + 0.3*x_test.p_global)


#helper.predict(x_test,zip_dic,'regionidzip',cols_input)
#helper.predict(x_test,city_dic,'regionidcity',cols_input)
gc.collect()

prop.p_global.fillna(0,inplace=True)
prop.p_regionidzip.fillna(prop.p_global,inplace=True)
prop.p_regionidcity.fillna(prop.p_global,inplace=True)


#0.068731898068767602
#mean_absolute_error(x_test.logerror,x_test.p_regionidzip)
#0.068367523124861787
#mean_absolute_error(x_test.logerror,x_test.p_regionidcity)

#0.07480232494579743 with x_test ==12
#0.06832999047435187
#mean_absolute_error(x_test.logerror,x_test.p_global_woc)
#0.068347084915558215
# mean_absolute_error(x_test.logerror,\
#                     .5*x_test.p_global +\
#                     .5*x_test.p_regionidcity\
#                     )

#0.068352563174374367
# mean_absolute_error(x_test.logerror,\
#                     .6*x_test.p_global +\
#                     .1*x_test.p_regionidzip +\
#                     .3*x_test.p_regionidcity\
#                     )

# prop['result'] = .3*prop['p_global'] + .4*prop['p_regionidzip']+.3*prop['p_regionidcity']

# mean_absolute_error(x_test.logerror,\
#                     .6*x_test.p_global +\
#                     .1*x_test.p_regionidzip +\
#                     .3*x_test.p_regionidcity\
#                     )



#Base result: 0.065555229888043298 - with abs(logerror) <=0.3
#0.065466586240686742


# params = {'n_estimators':[30,50,60,120,150,200,500]\
#     ,'max_bin':[255,512,1024,2048,4096,8192,16000,25000]\
#     ,'min_child_samples':[50,70,100,150,200]\
#     ,'num_leaves':[7,15,31,41,61]\
#     ,'learning_rate':[0.001,0.005,0.01,0.05,0.1,0.5]\
#     ,'objective':['mse','mae','huber','fair']\
#     , 'reg_alpha':[0.001,0.005,0.01,0.05,0.5,1,2,4]\
#     ,'reg_lambda':[0.001,0.005,0.01,0.05,0.5,1,2,4]\
#         }
#
# gs = GridSearchCV(LGBMRegressor(),params,scoring='neg_mean_absolute_error')
# gs.fit(x_train[cols_input],y_train)

# lgmr = LGBMRegressor(n_estimators=100,objective='mae', reg_alpha=20,max_bin=500\
#                      ,min_child_samples=100,num_leaves=31\
#                      ,reg_lambda=0,subsample_for_bin=10000)

#For m ==12
# lgmr = LGBMRegressor(n_estimators=300,objective='mae', reg_alpha=5,max_bin=500\
#                      ,min_child_samples=10,num_leaves=31,reg_lambda=0,subsample_for_bin=100000)

#Submission

gc.collect()
#prop = pd.read_csv('data/pp_properties_2016.csv')
prop= pickle.load(open('data/pp_properties_2016.pickle','rb'))
prop.fillna(0,inplace=True)
gc.collect()
reload(features)
features.create_features(prop)
gc.collect()
utils.predict(lgmr_woc,prop,cols_input,'p_global_woc',batch_size=100000)
score = prop['p_global_woc']
sub=pd.DataFrame()
sub['ParcelId'] = prop['parcelid']
sub['201610'] = [float(format(x, '.4f')) for x in score]
sub['201611'] = sub['201610']
sub['201612'] = sub['201610']
sub['201710'] = 0
sub['201711'] = 0
sub['201712'] = 0

sub.to_csv('ana/results40.csv',index=False)