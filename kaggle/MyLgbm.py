# With Gradient boosting - giving best results yet

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features2 as features2
import kaggle.features3 as features3
import kaggle.features4 as features4
import kaggle.features5 as features5
from importlib import reload
import gc
from types import SimpleNamespace
import kaggle.LGM_2 as helper
import kaggle.utils as utils
import pickle
from sklearn.linear_model import Ridge,LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import kaggle.alterable_features as alt_feature
from catboost import CatBoostRegressor
from mlxtend.regressor import StackingRegressor
from sklearn.externals import joblib
from mlens.ensemble import Subsemble
import traceback

pd.options.display.width = 2000
pd.options.display.max_columns=100

class MyLgbmR(LGBMRegressor):

    def __new__(cls,**kwds):
        return super().__new__(cls)


    def __init__(self,**kwds):
        super().__init__(**kwds)

    def set_fit_params_calc_func(self,func):
        self.set_params(fit_params_calculator=func)

    def fit(self,X,y,**kwds):
        params = self.get_params(deep=False)

        if hasattr(self,'fit_params_calculator'):
            fit_params_calculator = params['fit_params_calculator']
            delattr(self,'fit_params_calculator')

            if 'fit_params_calculator' in self.other_params:
                del self.other_params['fit_params_calculator']

            fit_params=fit_params_calculator(X)
            r = super().fit(X,y,**fit_params)
            self.set_params(**fit_params)
            return r
        else:
            super().fit(X,y)

# ens = Subsemble()
# est1 = mylgm.MyLgbmR()
# est1.set_fit_params(eval_metric='mae',early_stopping_rounds=20,eval_set=(ins.x_val[ins.cols_input],ins.x_val.logerror))
# ens.add(estimators=[est1])
# meta = mylgm.MyLgbmR()
# ens.add_meta(meta)
# ens.fit(ins.x_train[ins.cols_input],ins.x_train.logerror)


