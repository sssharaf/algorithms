import numpy as np
import pandas as pd
import gc
from scipy import stats
from sklearn.neighbors import  KernelDensity
from statsmodels.nonparametric.kde import KDEUnivariate

# Copy of features 2

def add_feature(prop_df):
    prop_df['bath_diff'] = prop_df['bathroomcnt'] - prop_df['fullbathcnt']

    # Number of properties in the zip
    zip_count = prop_df['regionidzip'].value_counts().to_dict()
    prop_df['N-zip_count'] = prop_df['regionidzip'].map(zip_count)

    # Number of properties in the city
    city_count = prop_df['regionidcity'].value_counts().to_dict()
    prop_df['N-city_count'] = prop_df['regionidcity'].map(city_count)

    # cols  = prop_df.columns.tolist()
    # a = pd.DataFrame()
    #
    # for i in range(0,len(cols)):
    #     colname1 = cols[i]
    #     for j in range(i,len(cols)):
    #         colname2 = cols[j]
    #         new_colname = "%s_minus_%s"%(colname1,colname2)
    #         a[new_colname] = np.float16(prop_df[colname1] - prop_df[colname2])
    #         print("Created column %s"%new_colname)


def remove_feature(prop_df):
    # cols_to_drop = prop_df.filter(regex='kde$').columns.tolist()
    # cols_to_drop.extend(prop_df.filter(regex='scaled').columns.tolist())
    # prop_df.drop(axis=1,labels=cols_to_drop,inplace=True)

    cols_to_drop = ['finishedsquarefeet13','finishedsquarefeet6','fips'\
        ,'rawcensustractandblock']
    #cols_to_drop.extend(prop_df.filter(regex='^regionid').columns.tolist())
    #print("Removing features : %s"%(cols_to_drop))
    #prop_df.drop(axis=1, labels=cols_to_drop, inplace=True)

