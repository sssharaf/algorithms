# With Gradient boosting - giving best results yet

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features as features
from importlib import reload
import gc

gc.collect()

pd.options.display.width = 2000

prop = pd.read_csv('data/pp_properties_2016.csv')
reload(features)
features.create_features(prop)

train = pd.read_csv("data/train_2016_v2.csv",parse_dates=['transactiondate'])
train['month'] =  train['transactiondate'].apply(lambda x: x.month)

train_df = train.merge(prop, how='left', on='parcelid')

cols = train_df.columns
cols_input = cols.drop(['parcelid', 'logerror', 'transactiondate','month'])

x_train = train_df.query('month <12 and abs(logerror) <=0.3')
y_train = train_df.query('month <12 and abs(logerror) <=0.3')["logerror"].values.astype(np.float32)
x_test = train_df.query('month ==12')
y_test = train_df.query('month ==12')["logerror"].values.astype(np.float32)

reload(preprocess)
cat_features = preprocess.find_categorical_features(train_df)

#065047024529733433 for m<10 and abs ==0.3
lgmr = LGBMRegressor(n_estimators=300,objective='mae', reg_alpha=20,max_bin=512\
                     ,min_child_samples=50,num_leaves=31)

lgmr.fit(x_train[cols_input], y_train,categorical_feature=cat_features,eval_set=(x_test[cols_input],y_test)\
         ,eval_metric='mae',early_stopping_rounds=20)

result_lgr = lgmr.predict(x_test[cols_input])
mean_absolute_error(x_test['logerror'],result_lgr)

#Base result: 0.065555229888043298 - with abs(logerror) <=0.3
#0.065466586240686742

imp = pd.DataFrame(data={'features':cols_input,'importance':lgmr.feature_importances_})
imp.sort_values(by='importance')


# params = {'n_estimators':[30,50,60,120,150,200,500]\
#     ,'max_bin':[255,512,1024,2048,4096,8192,16000,25000]\
#     ,'min_child_samples':[50,70,100,150,200]\
#     ,'num_leaves':[7,15,31,41,61]\
#     ,'learning_rate':[0.001,0.005,0.01,0.05,0.1,0.5]\
#     ,'objective':['mse','mae','huber','fair']\
#     , 'reg_alpha':[0.001,0.005,0.01,0.05,0.5,1,2,4]\
#     ,'reg_lambda':[0.001,0.005,0.01,0.05,0.5,1,2,4]\
#         }
#
# gs = GridSearchCV(LGBMRegressor(),params,scoring='neg_mean_absolute_error')
# gs.fit(x_train[cols_input],y_train)

# lgmr = LGBMRegressor(n_estimators=100,objective='mae', reg_alpha=20,max_bin=500\
#                      ,min_child_samples=100,num_leaves=31\
#                      ,reg_lambda=0,subsample_for_bin=10000)

#For m ==12
# lgmr = LGBMRegressor(n_estimators=300,objective='mae', reg_alpha=5,max_bin=500\
#                      ,min_child_samples=10,num_leaves=31,reg_lambda=0,subsample_for_bin=100000)

#Submission
prop_result = lgmr.predict(prop[cols_input])

sub=pd.DataFrame()
sub['ParcelId'] = prop['parcelid']
sub['201610'] = [float(format(x, '.4f')) for x in prop_result]
sub['201611'] = sub['201610']
sub['201612'] = sub['201610']
sub['201710'] = 0
sub['201711'] = 0
sub['201712'] = 0

sub.to_csv('ana/results7.csv',index=False)