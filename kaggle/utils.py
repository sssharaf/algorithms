from typing import List

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features as features
from importlib import reload
import gc
from types import SimpleNamespace

def predict(estimator,props_df,cols_input,result_col_name,batch_size=50000):
    pidx = props_df.index.tolist()
    tot_len = len(pidx)
    start =0
    props_df[result_col_name]=np.nan
    gc.collect()
    while start < tot_len:
        end = min(start + batch_size, tot_len)
        idx = pidx[start:end]
        print("Predicting from idx %s to %s"%(idx[0],idx[len(idx)-1]))
        props_df.loc[props_df.index.isin(idx),result_col_name] = \
            estimator.predict(props_df.loc[props_df.index.isin(idx),cols_input])
        start = start + batch_size
        gc.collect()

def predict_linear(estimator,pf,props_df,cols_input,result_col_name,batch_size=50000):
    pidx = props_df.index.tolist()
    tot_len = len(pidx)
    start =0
    props_df[result_col_name]=np.nan
    gc.collect()
    while start < tot_len:
        end = min(start + batch_size, tot_len)
        idx = pidx[start:end]
        print("Predicting from idx %s to %s"%(idx[0],idx[len(idx)-1]))
        props_df.loc[props_df.index.isin(idx),result_col_name] = \
            estimator.predict(pf.transform(props_df.loc[props_df.index.isin(idx),cols_input]))
        start = start + batch_size
        gc.collect()

def find_col_index(cols:List, query):
    if isinstance(query,list):
        return [ cols.index(c) for c in query]
    else:
        return cols.index(query)

def combine_results(fileno):
    res_2016 = pd.read_csv("ana/results%s_2016.csv"%fileno)
    res_2017 = pd.read_csv("ana/results%s_2017.csv" % fileno)

    res = res_2016.merge(res_2017,on='ParcelId')
    res.to_csv("ana/results%s.csv"%fileno,index=False)
