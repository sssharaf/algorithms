import numpy as np
import pandas as pd
import scipy
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
# from xgboost import  XGBRegressor as XGBR
from sklearn.neighbors import LSHForest as LSH
from sklearn.ensemble import GradientBoostingRegressor as GBR
from sklearn.neighbors import NearestNeighbors as NN
from kaggle import encode,preprocess
pd.options.display.width = 2000
pd.options.display.max_columns = 100

prop_df = pd.read_csv('data/properties_2016.csv')
train_df = pd.read_csv("data/train_2016_v2.csv",parse_dates=['transactiondate'])
train_df['month'] = train_df['transactiondate'].apply(lambda x: x.month)
train_df = train_df.merge(prop_df, how='left', on='parcelid')

m_all = train_df.query('month <=6 and abs(logerror) <=0.7')
m_all.reset_index(inplace=True)
m_all.drop(axis=1,labels=['index'],inplace=True)

preprocess.preprocess(m_all)

cols = train_df.columns
cols_input = cols.drop(['parcelid', 'logerror', 'transactiondate'])

m_all_enc = encode(m_all.copy()[cols_input])

nn = NN()
nn.fit(m_all_enc)

t = m_all.loc[m_all.parcelid==13092608,:]
nearest = nn.kneighbors(m_all_enc[t.index],return_distance=False)

from sklearn.preprocessing import RobustScaler,MinMaxScaler
rs = RobustScaler(with_centering=False)
mms = MinMaxScaler()

def scale( m) : return mms.fit_transform(rs.fit_transform(m))

m_all_enc_scaled = scale(m_all_enc)

nn.fit(m_all_enc_scaled)

nearest = nn.kneighbors(m_all_enc_scaled[t.index],return_distance=False)

jan_idx = m_all.query('month==1').index
feb_idx = m_all.query('month==2').index
mar_idx = m_all.query('month==3').index

m_jan_enc = m_all_enc[jan_idx]
m_feb_enc=m_all_enc[feb_idx]
m_mar_enc=m_all_enc[mar_idx]

nn_jan = NN()
nn_jan.fit(m_all_enc[jan_idx])
d,i=nn_jan.kneighbors(m_all_enc[feb_idx])
result_matrix = m_all.loc[jan_idx[i.reshape(-1)],'logerror'].values.reshape(-1,5)
result = np.median(result_matrix,axis=1)
mean_absolute_error(m_all.loc[feb_idx,'logerror'],result)

diff = m_all.loc[feb_idx,'logerror'] - result
from sklearn.ensemble import GradientBoostingRegressor as GBR
gbr = GBR(loss='lad',  min_samples_leaf=10,max_depth=5,n_estimators=100,subsample=0.8)
gbr.fit(m_all_enc[feb_idx],diff)
result_diff = gbr.predict(m_all_enc[feb_idx])
mean_absolute_error(m_all.loc[feb_idx,'logerror'],result+result_diff)

