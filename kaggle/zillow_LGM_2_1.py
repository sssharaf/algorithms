# With Gradient boosting - giving best results yet

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features2 as features2
import kaggle.features3 as features3
from importlib import reload
import gc
from types import SimpleNamespace
import kaggle.LGM_2 as helper
import kaggle.utils as utils
import pickle
from sklearn.linear_model import Ridge,LinearRegression
from sklearn.preprocessing import PolynomialFeatures

class LGM2_1(object):

    def load_prop(self):
        prop = pickle.load(open('data/pp_woc_properties_2016.pickle', 'rb'))
        prop.fillna(0, inplace=True)
        self.prop = prop

    def create_features(self,version=3):
        prop = self.prop
        if version ==2:
            reload(features2)
            features2.create_features(prop)
        elif version ==3:
            reload(features3)
            features3.create_features(prop)
        else :
            raise BaseException("Not supported")

    def prepare_train_df(self):

        prop = self.prop
        train = pd.read_csv("data/train_2016_v2.csv", parse_dates=['transactiondate'])
        train['month'] = train['transactiondate'].apply(lambda x: x.month)
        train_df = train.merge(prop, how='left', on='parcelid')
        #del prop
        gc.collect()

        reload(preprocess)
        cat_features = preprocess.find_categorical_features(train_df)
        cols = train_df.columns
        cols_input = cols.drop(['parcelid', 'logerror', 'transactiondate', 'month'])

        self.train_df = train_df
        self.cols_input = cols_input
        self.cat_features = cat_features

    def create_sets(self):
        train_df = self.train_df
        x_train = train_df.query('month <=7 and abs(logerror) <=2.0')
        x_val =  train_df.query('month ==8')
        x_test = train_df.query('month ==9')
        self.x_train = x_train
        self.x_val = x_val
        self.x_test = x_test

    def create_sets_for_submission(self):
        train_df = self.train_df
        x_train = train_df.query('month <=11 and abs(logerror) <=2.0')
        x_val = train_df.query('month ==12')
        return x_train,x_val

    def _train(self, x_train = None, x_val=None):
        x_train = self.x_train if x_train is None else x_train
        x_val = self.x_val if x_val is None else x_val

        cols_input = self.cols_input

        lgmr_woc = LGBMRegressor(n_estimators=1000, objective='mae', reg_alpha=20, max_bin=100 \
                                 , min_child_samples=50, num_leaves=512\
                                 , learning_rate=0.2
                                 ,subsample=0.8)

        lgmr_woc.fit(x_train[cols_input], x_train['logerror'], eval_set=(x_val[cols_input], x_val['logerror']) \
                     , eval_metric='mae', early_stopping_rounds=100, sample_weight=x_train['month'])

        lr = LinearRegression()
        lr.fit(x_train[cols_input], x_train['logerror'])

        return lgmr_woc

    def _train_linear(self,x_train=None):

        x_train = self.x_train if x_train is None else x_train
        cols_input = self.cols_input

        lr = LinearRegression()
        lr.fit(x_train[cols_input], x_train['logerror'])
        return lr

    def train(self, x_train=None, x_val=None):
        lgmr_woc = self._train(x_train,x_val)
        self.lgmr_woc = lgmr_woc
        gc.collect()

    def test(self):
        # Base: 0.06591372710093496 with abs(error) <2 and with feature2
        # 0.065946965740963884 with abs(error) <2 and with feature3
        # 0.065718084534645699 with abs(error) <2, with feature2 woc lgm
        # 0.065570623438523837 with abs(error) <2, with feature2 woc lgm w linear regr
        utils.predict(self.lgmr_woc, self.x_test, self.cols_input, 'p_global_woc')
        res = mean_absolute_error(self.x_test.logerror, self.x_test.p_global_woc)
        print(res)

        lr = self._train_linear()
        linear_score = lr.predict(self.x_test[self.cols_input])
        mix = 0.8*self.x_test.p_global_woc + 0.2* linear_score
        res = mean_absolute_error(self.x_test.logerror,mix)
        print(res)


    def run(self,fv=3,lf=False,sf=True):

        ff = "data/pp_features%s_properties_2016.pickle"%(fv)

        if lf :
            self.prop = pickle.load(open(ff,"rb"))
        else:
            self.load_prop()
            self.create_features(fv)
            if sf:
                print("Saving to file %s" % (ff), end="...")
                pickle.dump(self.prop, open(ff, 'wb'))
                print("saved.")

        self.prepare_train_df()
        self.create_sets()
        self.train()
        self.test()

    def show_imp(self):
        cols_input = self.cols_input
        lgmr_woc = self.lgmr_woc
        imp = pd.DataFrame(data={'features': cols_input, 'importance': lgmr_woc.feature_importances_})
        imp.sort_values(by='importance')

    def submit(self,fileno):
        prop = self.prop
        cols_input = self.cols_input

        x_train,x_val = self.create_sets_for_submission()

        print("Will train and make prediction using LGM model")
        lgmr_woc = self._train(x_train,x_val)
        utils.predict(lgmr_woc, prop, cols_input, 'p_global_woc', batch_size=100000)

        print("Will train and make prediction using linear model")
        lr = self._train_linear(x_train)
        linear_score = lr.predict(prop[cols_input])

        score = 0.8*prop['p_global_woc'] + 0.2*linear_score

        sub = pd.DataFrame()
        sub['ParcelId'] = prop['parcelid']
        sub['201610'] = [float(format(x, '.4f')) for x in score]
        sub['201611'] = sub['201610']
        sub['201612'] = sub['201610']
        sub['201710'] = 0
        sub['201711'] = 0
        sub['201712'] = 0

        file_name ="ana/results%s.csv"%(fileno)
        sub.to_csv(file_name, index=False)
pass

pd.options.display.width = 2000
