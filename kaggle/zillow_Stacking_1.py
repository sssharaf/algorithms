# With Gradient boosting - giving best results yet

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features2 as features2
import kaggle.features3 as features3
import kaggle.features4 as features4
import kaggle.features5 as features5
from importlib import reload
import gc
from types import SimpleNamespace
import kaggle.LGM_2 as helper
import kaggle.utils as utils
import pickle
from sklearn.linear_model import Ridge,LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import kaggle.alterable_features as alt_feature
from catboost import CatBoostRegressor
from mlxtend.regressor import StackingRegressor
from sklearn.externals import joblib
pd.options.display.width = 2000
pd.options.display.max_columns=100

class Stack_1(object):

    def load_prop(self):
        prop = pickle.load(open('data/pp_woc_properties_2016.pickle', 'rb'))
        prop.fillna(0, inplace=True)
        self.prop = prop

    def create_features(self,version=3):
        prop = self.prop
        if version ==2:
            reload(features2)
            features2.create_features(prop)
        elif version ==3:
            reload(features3)
            features3.create_features(prop)
        elif version == 4:
            reload(features4)
            features4.create_features(prop)
        elif version == 5:
            reload(features5)
            features5.create_features(prop)
        else :
            raise BaseException("Not supported")

    def prepare_train_df(self):

        prop = self.prop
        train = pd.read_csv("data/train_2016_v2.csv", parse_dates=['transactiondate'])
        train['month'] = train['transactiondate'].apply(lambda x: x.month)
        train_df = train.merge(prop, how='left', on='parcelid')
        #del prop
        gc.collect()

        self.train_df = train_df


    def create_sets(self):
        train_df = self.train_df
        x_train = train_df.query('month <=10 and abs(logerror) <=0.8')
        x_val =  train_df.query('month ==11')
        x_test = train_df.query('month ==12')
        self.x_train = x_train
        self.x_val = x_val
        self.x_test = x_test

    def create_sets_for_submission(self):
        train_df = self.train_df
        x_train = train_df.query('month <=11 and abs(logerror) <=2.0')
        x_val = train_df.query('month ==12')
        return x_train,x_val

    def add_month_dependent_feature(self, x_train=None, x_val=None, x_test=None):
        x_train = self.x_train if x_train is None else x_train
        x_val = self.x_val if x_val is None else x_val
        x_test = self.x_test if x_test is None else x_test

        mm = x_train.groupby(by=['regionidzip', 'month'])['logerror'] \
            .agg({
            #'regionidzip_count_logerror': np.count_nonzero
             'regionidzip_mean_logerror': np.mean
            #'regionidzip_median_logerror': np.median
            # ,'regionidzip_std_logerror': np.std
        })
        mm.fillna(100, inplace=True)

        mm_flat = mm.reset_index();
        mm_flat['month'] = mm_flat['month']+1
        mm = mm_flat.set_index(['regionidzip','month'])

        x_train = x_train.merge(mm_flat, how='left', on=['regionidzip', 'month'])
        x_train['regionidzip_mean_logerror'].fillna(0,inplace=True)

        #helper.use_linear_reg(mm, x_val, mm_stat_feature='regionidzip_count_logerror')
        #helper.use_linear_reg(mm, x_test, mm_stat_feature='regionidzip_count_logerror')
        helper.use_linear_reg(mm, x_val, mm_stat_feature='regionidzip_mean_logerror')
        helper.use_linear_reg(mm, x_test, mm_stat_feature='regionidzip_mean_logerror')
        x_val['regionidzip_mean_logerror'].fillna(0,inplace=True)
        x_test['regionidzip_mean_logerror'].fillna(0, inplace=True)

        self.x_train = x_train

    def select_features(self,x_train=None,set_to_self=True):
        x_train = self.x_train if x_train is None else x_train

        reload(preprocess)
        cat_features = preprocess.find_categorical_features(x_train)
        cols = x_train.columns
        cols_input = cols.drop(['parcelid', 'logerror', 'transactiondate', 'month'])

        cols_input = cols_input.tolist()
        cat_features = cat_features

        if(set_to_self):
            self.cols_input = cols_input
            self.cat_features = cat_features
        return(cols_input,cat_features)

    def train_catboost(self, x_train=None, x_val=None,use_ext_estimator=False):

        colname='p_catboost'
        x_train = self.x_train if x_train is None else x_train
        x_val = self.x_val if x_val is None else x_val

        cb = CatBoostRegressor(iterations=1000, learning_rate=0.005, depth=5, l2_leaf_reg=5
                               , loss_function='MAE'
                               , eval_metric='MAE', od_type='Iter', verbose=True)

        self.cb = cb
        if use_ext_estimator:
            cb.load_model('ana/cb.model')
            print("Loaded Catboost model")
            return cb,colname

        cat_features_idx = [self.cols_input.index(c) for c in self.cat_features]
        print("Fitting Catboost",end="...")
        cb.fit(x_train[self.cols_input],x_train.logerror
               ,cat_features=cat_features_idx
               ,eval_set=(x_val[self.cols_input],x_val.logerror)
               )

        cb.save_model('ana/cb.model')
        print("done")
        return cb, colname

    def train_lgbr_month(self, x_train = None, x_val=None,use_ext_estimator=False,objective='mae'):
        print("Traning LGBR month model")

        colname= 'p_lgmr_month'

        if use_ext_estimator:
            return joblib.load('ana/lgmr_month.pk'),colname

        x_train = self.x_train if x_train is None else x_train
        x_val = self.x_val if x_val is None else x_val
        cols_input = self.cols_input

        lgmr_woc = LGBMRegressor(n_estimators=4000, objective=objective, reg_alpha=5, max_bin=100 \
                                 ,reg_lambda=20
                                 ,min_child_samples=50, num_leaves=512\
                                 ,learning_rate=0.3
                                 ,subsample=0.8
                                 ,colsample_bytree=0.3
                                 )
        self.lgbr_month = lgmr_woc
        print("Fitting LGMR", end="...")
        lgmr_woc.fit(x_train[cols_input], x_train['logerror']
                     , sample_weight=x_train['month']
                     , eval_set=(x_val[cols_input], x_val['logerror']) \
                     #, eval_sample_weight=[x_val['month']]
                     #,categorical_feature=self.cat_features
                     , eval_metric='mae', early_stopping_rounds=100
                     )
        joblib.dump(lgmr_woc, 'ana/lgmr_month.pk')

        print("done")
        return lgmr_woc, colname

    def tr(self,x):
            x = abs(x)
            if x <= 0.05:
                return 10
            elif x <= 0.1:
                return 8
            elif x <=0.2 :
                return 6
            elif x <= 0.4:
                return 3
            return 1

    def train_lgbr_abslog(self, x_train=None, x_val=None,use_ext_estimator=False):
        print("Traning LGBR log model " )

        colname= 'p_lgmr_abslog'

        if use_ext_estimator:
            return joblib.load('ana/lgmr_abslog.pk'), colname

        x_train = self.x_train if x_train is None else x_train
        x_val = self.x_val if x_val is None else x_val
        cols_input = self.cols_input

        lgmr_woc = LGBMRegressor(n_estimators=4000, objective='mae', reg_alpha=5, max_bin=100 \
                                 , reg_lambda=20
                                 , min_child_samples=100, num_leaves=512 \
                                 , learning_rate=0.5
                                 , subsample=0.9
                                 , colsample_bytree=0.6
                                 , subsample_freq=40
                                 # ,boosting_type='dart'
                                 )


        trn_weights = x_train.logerror.transform(self.tr)
        val_weights = x_val.logerror.transform(self.tr)

        self.lgbr_abslog = lgmr_woc



        print("Fitting LGMR", end="...")
        lgmr_woc.fit(x_train[cols_input], x_train['logerror']
                     , sample_weight=trn_weights
                     , eval_set=(x_val[cols_input], x_val['logerror']) \
                     #, eval_sample_weight=[val_weights]
                     # ,categorical_feature=self.cat_features
                     , eval_metric='mae', early_stopping_rounds=100
                     )
        joblib.dump(lgmr_woc, 'ana/lgmr_abslog.pk')

        print("done")
        return lgmr_woc, colname

    def train_lgbr_county(self, county, x_train = None, x_val=None):
        print("Traning LGBR county model for county : %s"%county)

        x_train = self.x_train if x_train is None else x_train
        x_val = self.x_val if x_val is None else x_val
        cols_input = self.cols_input

        lgmr_woc = LGBMRegressor(n_estimators=4000, objective='mae', reg_alpha=2, max_bin=20 \
                                 ,reg_lambda=5
                                 ,min_child_samples=100, num_leaves=512\
                                 ,learning_rate=0.3
                                 ,subsample=0.9
                                 ,colsample_bytree=0.2
                                 ,subsample_freq=40
                                 #,boosting_type='dart'
                                 )

        trn_weights = x_train.regionidcounty.transform(lambda x: 0.9 if x == county else .1)
        val_weights = x_val.regionidcounty.transform(lambda x: 0.9 if x == county else .1)
        trn_weights = trn_weights * x_train['month']
        val_weights = val_weights * x_val['month']

        self.lgbr = lgmr_woc
        print("Fitting LGMR", end="...")
        lgmr_woc.fit(x_train[cols_input], x_train['logerror']
                     , sample_weight=trn_weights
                     , eval_set=(x_val[cols_input], x_val['logerror']) \
                     , eval_sample_weight=[val_weights]
                     #,categorical_feature=self.cat_features
                     , eval_metric='mae', early_stopping_rounds=100
                     )
        print("done")
        return lgmr_woc,'p_lgmr_county_%s'%county

    def train_lgbr_super(self, x_train = None, x_val=None):
        print("Traning LGBR SUPER model")
        x_train = self.x_train if x_train is None else x_train
        x_val = self.x_val if x_val is None else x_val
        cols_input,cat_features = self.select_features(x_train,set_to_self=False)
        cols_input.remove('fold')

        lgmr_woc = LGBMRegressor(n_estimators=4000, objective='mae', reg_alpha=5, max_bin=100 \
                                 ,reg_lambda=20
                                 ,min_child_samples=100
                                 , num_leaves=512
                                 ,learning_rate=0.05
                                 ,subsample=0.7
                                 ,colsample_bytree=0.2
                                 ,subsample_freq=50
                                 )

        self.super_estimator = lgmr_woc

        print("Fitting LGMR", end="...")
        lgmr_woc.fit(x_train[cols_input], x_train['logerror']
                     ,sample_weight=x_train['month']
                     , eval_set=(x_val[cols_input], x_val['logerror'])\
                     #,categorical_feature=self.cat_features
                     ,eval_metric='mae'
                     ,early_stopping_rounds=100
                     )
        print("done")
        return lgmr_woc

    def _train_linear(self,x_train=None,use_ext_estimator=False):

        colname = 'p_linear'
        if use_ext_estimator:
            self.pf = joblib.load('ana/lr_pf.pk')
            return joblib.load('ana/lr.pk'), colname

        x_train = self.x_train if x_train is None else x_train
        cols_input = self.cols_input

        lr = LinearRegression()
        #0.0664051989931 with alpha = 0.2
        #0.0664081178199 with alpha = 0.5
        lr = Ridge(alpha=5,normalize=True)
        pf = PolynomialFeatures(interaction_only=True,degree=2)
        print("Fitting Linear model", end="...")
        f = pf.fit_transform(x_train[cols_input])
        self.pf = pf
        lr.fit(f, x_train['logerror'])
        self.lr = lr
        joblib.dump(lr, 'ana/lr.pk')
        joblib.dump(pf, 'ana/lr_pf.pk')
        print("done")
        return lr,colname


    def train_linear_meta(self,cols_input,x_train=None,use_ext_estimator=False):

        colname = 'p_linear_meta'

        if use_ext_estimator:
            self.pf = joblib.load('ana/lr_meta_pf.pk')
            return joblib.load('ana/lr_meta.pk'), colname

        x_train = self.x_train if x_train is None else x_train

        lr = LinearRegression()
        lr = Ridge(alpha=5,normalize=True)
        pf = PolynomialFeatures(interaction_only=True,degree=2)
        print("Fitting Linear Meta model", end="...")
        f = pf.fit_transform(x_train[cols_input])
        self.pf_meta = pf
        lr.fit(f, x_train['logerror'])
        self.lr_meta = lr
        joblib.dump(lr, 'ana/lr_meta.pk')
        joblib.dump(pf, 'ana/lr_meta_pf.pk')
        print("done")
        return lr,colname


    def test(self):
        # Base: 0.06591372710093496 with abs(error) <2 and with feature2
        # 0.065946965740963884 with abs(error) <2 and with feature3
        # 0.065718084534645699 with abs(error) <2, with feature2 woc lgm
        # 0.065570623438523837 with abs(error) <2, with feature2 woc lgm w linear regr

        # 0.0656925737718
        # 0.0661480375025
        # 0.0656010717034

        utils.predict(self.cb, self.x_test, self.cols_input, 'p_cb')
        res = mean_absolute_error(self.x_test.logerror, self.x_test.p_cb)
        print("Cat boost:%s" % res)

        utils.predict(self.lgbr, self.x_test, self.cols_input, 'p_global_woc')
        res = mean_absolute_error(self.x_test.logerror, self.x_test.p_global_woc)
        print("LGBR boost:%s" % res)

        utils.predict_linear(self.lr,self.pf, self.x_test, self.cols_input, 'p_lr')
        res =mean_absolute_error(self.x_test.logerror,self.x_test.p_lr)
        print("Linear boost:%s" % res)

        mix = 0.5*self.x_test.p_global_woc + 0.3*self.x_test.p_cb + 0.1* self.x_test.p_lr
        res = mean_absolute_error(self.x_test.logerror,mix)
        print("Mix boost:%s" % res)


        mean = self.x_train.logerror.mean()
        mean_mix = 0.1*mean + 0.9*mix
        res = mean_absolute_error(self.x_test.logerror, mean_mix)
        print(res)

    def run(self,fv=5,lf=True,sf=False,lm=None):

        ff = "data/pp_features%s_properties_2016.pickle"%(fv)

        if lf :
            self.prop = pickle.load(open(ff,"rb"))
        else:
            self.load_prop()
            self.create_features(fv)
            if sf:
                print("Saving to file %s" % (ff), end="...")
                pickle.dump(self.prop, open(ff, 'wb'))
                print("saved.")

        reload(alt_feature)
        alt_feature.add_feature(self.prop)
        alt_feature.remove_feature(self.prop)

        self.prepare_train_df()
        self.create_sets()
        #self.add_month_dependent_feature()
        self.select_features()
        self.train_catboost(lm=lm)
        self.train_lgbr()
        self._train_linear()
        self.test()


    def create_meta_features(self,x_train=None,x_train_on_1_fold=None,nfolds=3,use_ext_estimator=False):
        x_train = self.x_train if x_train is None else x_train
        x_train['fold'] =   x_train.index % nfolds

        for f in range(0,nfolds):
            sub_x_test = x_train.loc[x_train.fold == f]
            sub_x_train = x_train.loc[x_train.fold != f] if nfolds > 1 else x_train_on_1_fold

            cols_input_meta = []

            estimator, colname = self.train_catboost(sub_x_train, self.x_val, use_ext_estimator)
            utils.predict(estimator, sub_x_test, self.cols_input, colname)
            x_train.loc[x_train.fold == f, colname] = sub_x_test[colname]
            utils.predict(estimator, self.x_val, self.cols_input, colname)
            cols_input_meta.append(colname)

            estimator,colname = self.train_lgbr_month(sub_x_train,self.x_val,use_ext_estimator)
            utils.predict(estimator,sub_x_test,self.cols_input,colname)
            x_train.loc[x_train.fold == f,colname] = sub_x_test[colname]
            utils.predict(estimator, self.x_val, self.cols_input, colname)
            cols_input_meta.append(colname)

            estimator,colname = self.train_lgbr_abslog(sub_x_train,self.x_val,use_ext_estimator)
            utils.predict(estimator,sub_x_test,self.cols_input,colname)
            x_train.loc[x_train.fold == f,colname] = sub_x_test[colname]
            utils.predict(estimator, self.x_val, self.cols_input, colname)
            cols_input_meta.append(colname)

            estimator,colname = self._train_linear(sub_x_train,use_ext_estimator)
            utils.predict_linear(estimator,self.pf,sub_x_test,self.cols_input,colname)
            x_train.loc[x_train.fold == f,colname] = sub_x_test[colname]
            utils.predict_linear(estimator, self.pf,self.x_val, self.cols_input, colname)
            cols_input_meta.append(colname)

            estimator, colname = self.train_linear_meta(cols_input_meta,sub_x_train, use_ext_estimator)
            utils.predict_linear(estimator, self.pf_meta, sub_x_test, cols_input_meta, colname)
            x_train.loc[x_train.fold == f, colname] = sub_x_test[colname]
            utils.predict_linear(estimator, self.pf_meta, self.x_val, cols_input_meta, colname)


            # estimator,colname = self.train_lgbr_county(3101,sub_x_train,self.x_val)
            # utils.predict(estimator,sub_x_test,self.cols_input,colname)
            # x_train.loc[x_train.fold == f,colname] = sub_x_test[colname]
            # utils.predict(estimator, self.x_val, self.cols_input, colname)
            #
            # estimator,colname = self.train_lgbr_county(1286,sub_x_train,self.x_val)
            # utils.predict(estimator,sub_x_test,self.cols_input,colname)
            # x_train.loc[x_train.fold == f,colname] = sub_x_test[colname]
            # utils.predict(estimator, self.x_val, self.cols_input, colname)
            #
            # estimator,colname = self.train_lgbr_county(2061,sub_x_train,self.x_val)
            # utils.predict(estimator,sub_x_test,self.cols_input,colname)
            # x_train.loc[x_train.fold == f,colname] = sub_x_test[colname]
            # utils.predict(estimator, self.x_val, self.cols_input, colname)


    def test_super(self,x_test =None):

        x_test = self.x_test if x_test is None else x_test

        cols_input, cat_features = self.select_features(self.x_train, set_to_self=False)
        cols_input.remove('fold')
        utils.predict(self.super_estimator, x_test, cols_input, 'p_super')

    def load_props_and_features(self,fv=5,lf=True,sf=False):
        ff = "data/pp_features%s_properties_2016.pickle" % (fv)
        if lf:
            self.prop = pickle.load(open(ff, "rb"))
        else:
            self.load_prop()
            self.create_features(fv)
            if sf:
                print("Saving to file %s" % (ff), end="...")
                pickle.dump(self.prop, open(ff, 'wb'))
                print("saved.")


    def run_w_stack(self, fv=5, lf=True, sf=False, lm=True):
        self.load_props_and_features(fv,lf,sf)
        reload(alt_feature)
        alt_feature.add_feature(self.prop)
        alt_feature.remove_feature(self.prop)

        self.prepare_train_df()
        self.create_sets()
        self.select_features()
        self.create_meta_features(nfolds=1,x_train_on_1_fold=self.x_train,use_ext_estimator=lm)
        self.train_lgbr_super()
        self.create_meta_features(self.x_test,x_train_on_1_fold=self.x_train,nfolds=1,use_ext_estimator=True)
        # 0.065599519988397817 with nfolds=1
        self.test_super()
        print(mean_absolute_error(self.x_test.logerror, self.x_test.p_super))

    def show_imp(self):
        cols_input = self.cols_input
        lgmr_woc = self.lgmr_woc
        imp = pd.DataFrame(data={'features': cols_input, 'importance': lgmr_woc.feature_importances_})
        print(imp.sort_values(by='importance'))

    def submit(self,fileno):
        prop = self.prop
        self.create_meta_features(nfolds=1,x_train=prop, x_train_on_1_fold=self.prop, use_ext_estimator=True)
        self.test_super(prop)

        sub = pd.DataFrame()
        sub['ParcelId'] = prop['parcelid']
        sub['201610'] = [float(format(x, '.4f')) for x in prop.p_super]
        sub['201611'] = sub['201610']
        sub['201612'] = sub['201610']
        sub['201710'] = 0
        sub['201711'] = 0
        sub['201712'] = 0

        file_name ="ana/results%s.csv"%(fileno)
        sub.to_csv(file_name, index=False)

    def search_best_params(self):

        x_train = self.x_train
        x_val = self.x_val
        cols_input = self.cols_input

        params = {'n_estimators':[100,500,1000,1500]\
            ,'max_bin':[25,50,100,255]\
            ,'min_child_samples':[10,30,50,100]\
            ,'num_leaves':[64,255,512,1024]\
            ,'learning_rate':[0.001,0.05,0.1,0.5]\
            ,'objective':['mae']\
            , 'reg_alpha':[0.5,5,10,20,40]\
            ,'subsample':[0.6,0.7,0.8,1.0]
            }

        fit_params = {
            'eval_set': (x_val[cols_input], x_val['logerror'])\
             ,'eval_metric':'mae'\
            ,'early_stopping_rounds': 100\
            ,'sample_weight': x_train['month']\
        }
        gs = GridSearchCV(LGBMRegressor(),param_grid=params,scoring='neg_mean_absolute_error'
                          ,fit_params=fit_params
                          ,n_jobs=4
                          )
        gs.fit(x_train[cols_input],x_train['logerror'])

        return gs
pass

pd.options.display.width = 2000
