import numpy as np
import pandas as pd
import scipy
import gc
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
# from xgboost import  XGBRegressor as XGBR
from sklearn.neighbors import LSHForest as LSH
from sklearn.ensemble import GradientBoostingRegressor as GBR
from sklearn.neighbors import NearestNeighbors as NN

#prop_df = pd.read_csv('data/properties_2016.csv')
#train_df = pd.read_csv("data/train_2016_v2.csv", parse_dates=['transactiondate'])
#train_df = train_df.merge(prop_df, how='left', on='parcelid')

def preprocess(train_df,lat_long_pp=False,categorize_cols = False):
    #train_df['null_cols_cnt']=train_df.apply(axis=1,func=lambda x: ([ pd.isnull(x[index]) for index in x.index ].count(True)) )

    # for c in train_df.columns:
    #     if train_df[c].dtype == 'float':
    #         train_df[c] = train_df[c].astype(np.float32)
    #     if train_df[c].dtype == 'int64':
    #         train_df[c] = train_df[c].astype(np.int32)

    if(lat_long_pp):
        train_df['latitude'] = train_df['latitude'] / 1e6
        train_df['longitude'] = train_df['longitude'] / 1e6

    train_df['hashottuborspa'] = train_df['hashottuborspa'].fillna(0)
    train_df['fireplaceflag'] = train_df['fireplaceflag'].fillna(0)
    train_df['taxdelinquencyflag'] = train_df['taxdelinquencyflag'].fillna(0)

    train_df.hashottuborspa = train_df.hashottuborspa.astype(np.int8)
    train_df.fireplaceflag = train_df.fireplaceflag.astype(np.int8)
    train_df['taxdelinquencyflag'].replace('Y', 1, inplace=True)
    train_df.taxdelinquencyflag = train_df.taxdelinquencyflag.astype(np.int8)
    train_df["propertycountylandusecode"].fillna('023A', inplace=True)
    train_df["propertyzoningdesc"].fillna('UNIQUE', inplace=True)

    # # Update null city
    # d = {i['regionidzip']: i['regionidcity'] for v, i in
    #      train_df.loc[~train_df['regionidcity'].isnull(), ['regionidzip', 'regionidcity']].iterrows()}
    #
    # train_df.loc[train_df['regionidcity'].isnull(), ['regionidcity']] = \
    #     train_df.loc[train_df['regionidcity'].isnull(), ['regionidcity', 'regionidzip']] \
    #         .apply(axis=1, func=lambda x: d[x['regionidzip']] if x['regionidzip'] in d else np.nan)

    df_with_ll = train_df.loc[~np.isnan(train_df['latitude']) \
                              & ~np.isnan(train_df['longitude']) \
                              & ~np.isnan(train_df['regionidzip']) \
                              & ~np.isnan(train_df['regionidcity']) \
        , ['latitude', 'longitude', 'regionidzip', 'regionidcity']]

    from sklearn.neighbors import NearestNeighbors

    # Fill regionidzip
    nn = NearestNeighbors(algorithm='ball_tree')
    nn.fit(df_with_ll.loc[:,['latitude','longitude']])
    nullzip = train_df.loc[np.isnan(train_df.regionidzip) \
                           & ~np.isnan(train_df['latitude'])\
                           & ~np.isnan(train_df['longitude'])\
        , ['regionidcity','regionidzip','latitude','longitude'] ]

    if(nullzip.size !=0):
        idx_zip = nn.kneighbors(nullzip.loc[:,['latitude','longitude']],n_neighbors=1,return_distance=False)
        train_df.loc[nullzip.index,'regionidzip'] = df_with_ll.iloc[idx_zip.reshape(-1)]['regionidzip'].values

    del nullzip
    gc.collect()

    # Fill regionidcity
    nullcity = train_df.loc[np.isnan(train_df.regionidcity) & ~np.isnan(train_df.latitude) & ~np.isnan(train_df.longitude), ['regionidcity','regionidzip','latitude','longitude'] ]
    if(nullcity.size !=0):
        idx_city = nn.kneighbors(nullcity.loc[:,['latitude','longitude']],n_neighbors=1,return_distance=False)
        train_df.loc[nullcity.index,'regionidcity']=df_with_ll.iloc[idx_city.reshape(-1)]['regionidcity'].values

    del nullcity
    del df_with_ll
    gc.collect()

    # Fill censuntrackandblock
    df_with_ll = train_df.loc[~np.isnan(train_df['latitude']) \
                              & ~np.isnan(train_df['longitude']) \
                              & ~np.isnan(train_df['censustractandblock']) \
        , ['latitude', 'longitude',  'censustractandblock']]
    nn = NearestNeighbors(algorithm='ball_tree')
    nn.fit(df_with_ll.loc[:,['latitude','longitude']])
    null_censustractandblock = train_df.loc[np.isnan(train_df.censustractandblock) & ~np.isnan(train_df.latitude) & ~np.isnan(train_df.longitude), ['censustractandblock','latitude','longitude'] ]
    if(null_censustractandblock.size!=0):
        idx_censustractandblock = nn.kneighbors(null_censustractandblock.loc[:,['latitude','longitude']],n_neighbors=1,return_distance=False)
        train_df.loc[null_censustractandblock.index,'censustractandblock'] = df_with_ll.iloc[idx_censustractandblock.reshape(-1)]['censustractandblock'].values

    del null_censustractandblock
    del df_with_ll
    gc.collect()

    # Fill regionidneighbourhood
    df_with_ll = train_df.loc[~np.isnan(train_df['latitude']) \
                              & ~np.isnan(train_df['longitude']) \
                              & ~np.isnan(train_df['regionidneighborhood']) \
        , ['latitude', 'longitude',  'regionidneighborhood']]
    nn = NearestNeighbors(algorithm='ball_tree')
    nn.fit(df_with_ll.loc[:,['latitude','longitude']])
    null_regionidneighborhood = train_df.loc[np.isnan(train_df.regionidneighborhood) & ~np.isnan(train_df.latitude) & ~np.isnan(train_df.longitude), ['regionidneighborhood','latitude','longitude'] ]
    if(null_regionidneighborhood.size!=0):
        idx_regionidneighborhood = nn.kneighbors(null_regionidneighborhood.loc[:,['latitude','longitude']],n_neighbors=1,return_distance=False)
        train_df.loc[null_regionidneighborhood.index,'regionidneighborhood'] = df_with_ll.iloc[idx_regionidneighborhood.reshape(-1)]['regionidneighborhood'].values

    del null_regionidneighborhood
    del df_with_ll
    gc.collect()

    train_df['yearbuilt'] = train_df['yearbuilt'].fillna(2017)
    train_df['unitcnt'] = train_df['unitcnt'].fillna(train_df['unitcnt'].mode()[0])

    train_df['taxvaluedollarcnt'].fillna(train_df['structuretaxvaluedollarcnt']+train_df['landtaxvaluedollarcnt'],inplace=True)
    train_df['structuretaxvaluedollarcnt'].fillna(train_df['taxvaluedollarcnt'] - train_df['landtaxvaluedollarcnt'],inplace=True)
    train_df['landtaxvaluedollarcnt'].fillna(train_df['taxvaluedollarcnt'] - train_df['structuretaxvaluedollarcnt'],inplace=True)

    float_cols = list(train_df.select_dtypes(include=['float']).columns)
    float_nan_col = []
    for column in float_cols:
        if (train_df[column].isnull().sum() > 0):
            float_nan_col.append(column)

    float_filled_cols = ['regionidcity', 'regionidneighborhood', 'regionidzip', 'unitcnt', 'censustractandblock',
                         'yearbuilt']

    count = 0
    for i in float_nan_col:
        if i not in float_filled_cols:
            train_df[i] = train_df[i].fillna(0)
            count += 1

    cols_to_drop = ['assessmentyear']
    train_df.drop(labels=cols_to_drop,axis=1,inplace=True)

    train_df.fillna(0, inplace=True)
    cols_category = []
    cols_category = train_df.filter(like='typeid').columns.tolist()
    cols_category.extend(train_df.filter(like='regionid').columns.values)

    _fillna_encode(train_df)

    if categorize_cols:
        for c in cols_category:
            train_df[c] = train_df[c].astype('category')



def _fillna_encode(prop):
    for c in prop.columns:
        if prop[c].dtype == 'object':
            lbl = LabelEncoder()
            lbl.fit(list(prop[c].values))
            prop[c] = lbl.transform(list(prop[c].values))

def find_categorical_features(df):

    cols_category = df.filter(like='typeid').columns.tolist()
    cols_category.extend(df.filter(regex='^regionid').columns.values)
    cols_category.extend(df.filter(regex='flag$').columns.values)
    return cols_category;