
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from types import SimpleNamespace

pd.options.display.width = 2000


def train_by_region(region_id,train_df,cols_input):
    rec_by_keys={}
    keys = train_df[region_id].unique()
    for k in keys:
        ns = SimpleNamespace()
        ns.train_df = train_df.loc[train_df[region_id]==k,:]
        ns.x_train = ns.train_df.query('month <9 and abs(logerror) <=0.3')[cols_input]
        ns.y_train = ns.train_df.query('month <9 and abs(logerror) <=0.3')["logerror"].values.astype(np.float32)
        if(len(ns.x_train) >0):
            rec_by_keys[k] =ns

    from sklearn.ensemble import GradientBoostingRegressor as GBR
    for k in keys:
        ns = rec_by_keys[k]
        x_train = ns.x_train
        y_train = ns.y_train
        gbr = GBR(loss='ls', min_samples_leaf=5, max_depth=2, n_estimators=150, subsample=1)
        gbr.fit(x_train, y_train)
        ns.gbr = gbr

    return rec_by_keys


def predict(region_id,rec_by_keys,x_test,cols_input):
    for k,ns in rec_by_keys.items():
        ns = rec_by_keys[k]
        x_test_by_k = x_test.loc[x_test[region_id] == k, :]
        if len(x_test_by_k) == 0:
            continue
        ns.x_test_by_k = x_test_by_k
        x_test.loc[x_test[region_id] == k, 'result_local'] = ns.gbr.predict(x_test_by_k[cols_input])


prop = pd.read_csv('data/properties_2016.csv')
train = pd.read_csv("data/train_2016_v2.csv",parse_dates=['transactiondate'])
train['month'] =  train['transactiondate'].apply(lambda x: x.month)

for c in prop.columns:
    prop[c] = prop[c].fillna(-1)
    if prop[c].dtype == 'object':
        lbl = LabelEncoder()
        lbl.fit(list(prop[c].values))
        prop[c] = lbl.transform(list(prop[c].values))

train_df = train.merge(prop, how='left', on='parcelid')

cols = train_df.columns
cols_input = cols.drop(['parcelid', 'logerror', 'transactiondate','month'])


x_test = train_df.query('month >=9')
y_test = train_df.query('month >=9')["logerror"].values.astype(np.float32)

region_id ='regionidzip'
rec_by_keys =train_by_region(region_id,train_df,cols_input)
predict(region_id,rec_by_keys,x_test,cols_input)


#Global
from sklearn.ensemble import GradientBoostingRegressor as GBR
x_train = train_df.query('month <9 and abs(logerror) <=0.3')[cols_input]
y_train = train_df.query('month <9 and abs(logerror) <=0.3')["logerror"].values.astype(np.float32)
# For outliers dataset
gbr = GBR(loss='ls',  min_samples_leaf=10,max_depth=5,n_estimators=100,subsample=0.8)
gbr.fit(x_train, y_train)
x_test['result_global'] = gbr.predict(x_test[cols_input])
mean_absolute_error(x_test['logerror'],x_test['result_global'])



#Base result: 0.065555229888043298 - with abs(logerror) <=0.3
mean_absolute_error(x_test['logerror'],x_test['result_local'])

x_test['result'] = x_test.apply(axis=1,func=lambda x: (x['result_local'] + x['result_global'])/2)
mean_absolute_error(x_test['logerror'],x_test['result'])


imp = pd.DataFrame(data={'features':cols_input,'importance':gbr.feature_importances_})
imp.sort_values(by='importance')

#Submission
prop_result_df = prop.copy()
prop_result_df['result_global'] = gbr.predict(prop[cols_input])


for k in keys:
    ns = rec_by_keys[k]
    x_test_by_k = prop_result_df.loc[prop_result_df['regionidzip'] == k, :]
    ns.x_test_by_k= x_test_by_k
    prop_result_df.loc[prop_result_df['regionidzip'] == k, 'result_local'] = ns.gbr.predict(x_test_by_k[cols_input])

prop_result_df['result_local'].fillna(0,inplace=True)

prop_result_df['result'] = prop_result_df.apply(axis=1,func=lambda x: (x['result_local'] + x['result_global'])/2)


sub=pd.DataFrame()
sub['ParcelId'] = prop_result_df['parcelid']
pred=prop_result_df['result']
sub['201610'] = [float(format(x, '.4f')) for x in pred]
sub['201611'] = sub['201610']
sub['201612'] = sub['201610']
sub['201710'] = 0
sub['201711'] = 0
sub['201712'] = 0

sub.to_csv('ana/results6.csv',index=False)