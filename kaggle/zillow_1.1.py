
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error

pd.options.display.width = 1000

prop = pd.read_csv('data/properties_2016.csv')
train = pd.read_csv("data/train_2016_v2.csv",parse_dates=['transactiondate'])
train['month'] =  train['transactiondate'].apply(lambda x: x.month)

for c in prop.columns:
    prop[c] = prop[c].fillna(-1)
    if prop[c].dtype == 'object':
        lbl = LabelEncoder()
        lbl.fit(list(prop[c].values))
        prop[c] = lbl.transform(list(prop[c].values))

train_df = train.merge(prop, how='left', on='parcelid')
cols = train_df.columns
cols_input = cols.drop(['parcelid', 'logerror', 'transactiondate','month'])

x_train = train_df.query('month <9 and abs(logerror) <=0.3')[cols_input]
y_train = train_df.query('month <9 and abs(logerror) <=0.3')["logerror"].values.astype(np.float32)
x_test = train_df.query('month >=9')
y_test = train_df.query('month >=9')["logerror"].values.astype(np.float32)

from sklearn.ensemble import GradientBoostingRegressor as GBR

# For outliers dataset
gbr = GBR(loss='ls',  min_samples_leaf=10,max_depth=5,n_estimators=100,subsample=0.8)
gbr.fit(x_train, y_train)

x_train1 = train_df.query('month <9 and abs(logerror) >0.3')[cols_input]
y_train1 = train_df.query('month <9 and abs(logerror) >0.3')["logerror"].values.astype(np.float32)
x_test1 = train_df.query('month >=9 and abs(logerror) >0.3')
y_test1 = train_df.query('month >=9 and abs(logerror) >0.3')["logerror"].values.astype(np.float32)
gbr1 = GBR(loss='ls',  min_samples_leaf=10,max_depth=5,n_estimators=100,subsample=0.8)
gbr1.fit(x_train1, y_train1)

x_train_class = train_df.query('month <9')[cols_input]
y_train_class = train_df.query('month <9')["logerror"]
y_train_class=y_train_class.apply(func= lambda x: 1 if abs(x)<=0.3 else 0)
from sklearn.ensemble import GradientBoostingClassifier as GBC
gbc = GBC(min_samples_split=10,min_samples_leaf=5)
gbc.fit(x_train_class,y_train_class)

x_test['class'] = gbc.predict(x_test[cols_input])
x_test['result_1'] = gbr.predict(x_test[cols_input])
x_test['result_2'] = gbr1.predict(x_test[cols_input])

x_train_rows = x_train.shape[0]
x_train1_rows = x_train1.shape[0]
total = x_train_rows + x_train1_rows
x_test['result'] = (x_train_rows/total)*x_test['result_1'] + (x_train1_rows/total)*x_test['result_2']

x_test['result'] = x_test.apply(axis=1,func=lambda x: x['result_1'] if (x['class'] == 1) else x['result_2']  )

#Base result: 0.065555229888043298
mean_absolute_error(x_test['logerror'],x_test['result'])

imp = pd.DataFrame(data={'features':cols_input,'importance':gbr.feature_importances_})
imp.sort_values(by='importance')

#Submission
prop_result_df = prop.copy()
prop_result_df['result'] = gbr.predict(prop[cols_input])

sub=pd.DataFrame()
sub['ParcelId'] = prop_result_df['parcelid']
pred=prop_result_df['result']
sub['201610'] = [float(format(x, '.4f')) for x in pred]
sub['201611'] = sub['201610']
sub['201612'] = sub['201610']
sub['201710'] = 0
sub['201711'] = 0
sub['201712'] = 0

sub.to_csv('ana/results5.csv',index=False)