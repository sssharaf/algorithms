import numpy as np
import pandas as pd
import gc
from scipy import stats
from sklearn.neighbors import  KernelDensity
from statsmodels.nonparametric.kde import KDEUnivariate

# Copy of features 2

def create_features(train_df):

    train_df.loc[train_df.roomcnt == 0, 'roomcnt'] = train_df.bedroomcnt + train_df.calculatedbathnbr
    train_df['additional_room'] = train_df.roomcnt - (train_df.bedroomcnt + train_df.calculatedbathnbr)
    train_df['landtax_to_lotsize'] = train_df.landtaxvaluedollarcnt / (train_df.lotsizesquarefeet + 1.0)
    train_df['structuretaxvaluedollarcnt_to_lotsize'] = \
        train_df.structuretaxvaluedollarcnt / (train_df.lotsizesquarefeet + 1.0)
    train_df['taxamount_to_lotsize'] = train_df.taxamount / (train_df.lotsizesquarefeet + 1.0)
    train_df['calculatedfinishedsquarefeet_to_roomcnt'] = train_df.calculatedfinishedsquarefeet / (train_df.roomcnt + 1)
    train_df['structuretax_to_calculatedfinishedsquarefeet'] = train_df.structuretaxvaluedollarcnt / (
        train_df.calculatedfinishedsquarefeet + 1.0)
    train_df['lotsize_minus_calculatedfinishedsquarefeet'] = train_df.lotsizesquarefeet - train_df.calculatedfinishedsquarefeet
    train_df['taxamount_to_taxvaluedollarcnt'] = train_df.taxamount / (train_df.taxvaluedollarcnt + 1)

    _create_feature_kde(train_df, 'regionidzip', 'yearbuilt')
    gc.collect()

    _create_feature_kde(train_df, 'regionidzip', 'finishedsquarefeet12')
    gc.collect()

    _create_feature_kde(train_df, 'regionidzip', 'taxamount')
    gc.collect()

    _create_feature_kde(train_df, 'regionidzip', 'structuretaxvaluedollarcnt')
    gc.collect()

    _create_feature_kde(train_df, 'regionidzip', 'lotsizesquarefeet')
    gc.collect()

    _create_feature_kde(train_df, 'regionidzip', 'taxamount_to_taxvaluedollarcnt')
    gc.collect()

def _create_feature_kde(train_df,groupbyKey, column):
    gc.collect()
    new_col_name = ("{0}_{1}_kde").format(column, groupbyKey)
    train_df[new_col_name] = 0.0
    print("[KDE]Creating new feature %s" % (new_col_name),end="\t\t...")

    # filtered_df = train_df.loc[
    #     (train_df[column] > np.nanpercentile(train_df[column], 10)) \
    #     & (train_df[column] < np.nanpercentile(train_df[column], 90)) \
    #         , [groupbyKey, column]]

    filtered_df = train_df

    city_structuretax_grp = filtered_df.groupby(by=groupbyKey)

    for k, v in city_structuretax_grp.groups.items():
        a = filtered_df.loc[v][column].values
        if a.size > 1:
            #kde = KernelDensity(kernel='gaussian',rtol=1,bandwidth=0.2,algorithm='ball_tree')
            kde = KDEUnivariate(a)
            #print("Fitting data for key %s(%s) with number of values %s"%(groupbyKey,k,a.size))
            #kde.fit(np.reshape(a,(-1,1)),fft=True)
            kde.fit()
            #print("Scoring samples...")
            #r = kde.score_samples(np.reshape(a,(-1,1)))
            # print("a")
            # print(a)
            # print("kde.support")
            # print(kde.support)
            # idx = kde.support.searchsorted(a)
            # print("idx")
            # print(kde.support[idx])
            r =kde.density[kde.support.searchsorted(a)]
            #print("Updating dataframe...")
            train_df.loc[v,new_col_name] = r
            del kde
            del r
        del a
        gc.collect()

    train_df[new_col_name].fillna(0, inplace=True)
    del filtered_df
    del city_structuretax_grp

    print("created")


def _create_feature_scaled(train_df,groupbyKey, column):
    gc.collect()
    new_col_name = ("{0}_{1}_scaled").format(column, groupbyKey)
    train_df[new_col_name] = 0.0
    print("[Scaled]Creating new feature %s" % (new_col_name),end="\t\t...")

    # filtered_df = train_df.loc[
    #     (train_df[column] > np.nanpercentile(train_df[column], 10)) \
    #     & (train_df[column] < np.nanpercentile(train_df[column], 90)) \
    #         , [groupbyKey, column]]

    filtered_df = train_df

    city_structuretax_grp = filtered_df.groupby(by=groupbyKey)

    from sklearn.preprocessing import RobustScaler
    for k, v in city_structuretax_grp.groups.items():
        a = filtered_df.loc[v][column].values
        if a.size > 1:
            rs = RobustScaler(with_centering=False)
            #print("Updating dataframe...")
            train_df.loc[v,new_col_name] = rs.fit_transform(a)
            del rs
        del a
        gc.collect()

    train_df[new_col_name].fillna(0, inplace=True)
    del filtered_df
    del city_structuretax_grp

    print("created.")
