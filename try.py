# With Gradient boosting - giving best results yet

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error
from kaggle.columns import column_types
from lightgbm.sklearn import LGBMRegressor
import kaggle.preprocess as preprocess
from sklearn.model_selection import GridSearchCV
import kaggle.features2 as features2
import kaggle.features3 as features3
import kaggle.features4 as features4
import kaggle.features5 as features5
from importlib import reload
import gc
from types import SimpleNamespace
import kaggle.LGM_2 as helper
import kaggle.utils as utils
import pickle
from sklearn.linear_model import Ridge,LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import kaggle.alterable_features as alt_feature
from catboost import CatBoostRegressor
from mlxtend.regressor import StackingRegressor
from sklearn.externals import joblib
from mlens.ensemble import Subsemble
import kaggle.MyLgbm as mylgm
import traceback
import kaggle.zillow_Stacking_2 as lgm

fileno=55

self = lgm.Stack_2()
self.run_w_stack(year=2016)
self.create_ensemble()

self.submit(fileno)
gc.collect()

self = lgm.Stack_2()
self.run_w_stack(year=2017)
self.create_ensemble()
self.submit(fileno)
gc.collect()

utils.combine_results(fileno)

