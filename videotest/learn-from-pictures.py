import numpy as np
import cv2
import os
import pandas as pd
import keras.layers as layers
from keras.models import Model
from sklearn.preprocessing import OneHotEncoder,LabelEncoder
import pickle
from keras.utils.np_utils import to_categorical
import keras.activations as activations
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
import keras.optimizers as optimizers
from keras.models import load_model
from keras import backend as K
import keras.losses as losses
from os import listdir
from os.path import isfile, join

def create_layer(input,conv_filters,conv_kernel,one_by_one_sz=None,normalize=False,pool_size = None):

    y = input

    if normalize:
        y = layers.BatchNormalization()(y)

    if one_by_one_sz is not None:
        y = layers.Conv2D(filters=one_by_one_sz, kernel_size=(1, 1),padding='same',activation='relu')(y)

    y = layers.Conv2D(filters=conv_filters, kernel_size=(conv_kernel,conv_kernel),padding='same')(y)

    y = layers.Activation(activation='relu')(y)

    if pool_size is not None:
        y = layers.MaxPool2D(pool_size=(pool_size,pool_size))(y)

    return y

persons = ['sharaf','parmee','no-one']

dirnames = { p:'../data/vidtest/%s'%(p) for p in persons}

noi = 150
width =250
height=250
X_train_base = np.zeros((noi*len(persons),height,width,3),dtype=np.uint8)
y = []
x_i = 0
for cnt in range(0,noi):
    for p in persons:
        fn = '%s/%s.png'%(dirnames[p],cnt)
        print("Loading file:%s"%(fn))
        img = cv2.imread(fn)

        r , c= img.shape[0],img.shape[1]
        rf = int(0.2*r)
        cf = int(0.2*r)
        img = img[rf:r-rf, cf:r-cf,:]
        # cv2.imshow('frame',img)
        # cv2.waitKey(0)
        img = cv2.resize(img,(height,width),interpolation=cv2.INTER_CUBIC)
        X_train_base[x_i] = img
        x_i = x_i +1
        y.append( p)

y_le = LabelEncoder()
y_le.fit(np.reshape(y,(-1,)))
pickle.dump(y_le,open('../data/y_le.pickle','wb'))

y_le_t = np.reshape(y_le.transform(y),(-1,1))
y_ohe = OneHotEncoder(sparse=False)
y_ohe.fit(y_le_t)
y_train_base = y_ohe.transform(y_le_t)

X_train, X_val,y_train,y_val = train_test_split(X_train_base,y_train_base,test_size=0.2)
datagen = ImageDataGenerator(
    featurewise_center=False,
    featurewise_std_normalization=True,
    rotation_range=30,
    rescale=1./255,
    width_shift_range=0.2,
    height_shift_range=0.2,
    horizontal_flip=True,
    vertical_flip=False,
    shear_range=20,
    zoom_range=0.4
    )


datagen.fit(X_train)

input = layers.Input(shape=(height,width,3))
y = create_layer(input,conv_filters=64,conv_kernel=5,one_by_one_sz=None,normalize=True,pool_size=2)
y = layers.Dropout(rate=0.4)(y)
y = create_layer(y,conv_filters=32,conv_kernel=3,one_by_one_sz=32,normalize=False,pool_size=2)
y = layers.Dropout(rate=0.4)(y)
y = create_layer(y,conv_filters=32,conv_kernel=3,one_by_one_sz=16,normalize=False,pool_size=2)
y = layers.Dropout(rate=0.4)(y)
y = create_layer(y,conv_filters=16,conv_kernel=3,one_by_one_sz=8,normalize=False,pool_size=2)
y = layers.Dropout(rate=0.4)(y)
y = create_layer(y,conv_filters=3,conv_kernel=2,one_by_one_sz=4,normalize=False,pool_size=2)

y = layers.GlobalAveragePooling2D()(y)
#y = layers.Dropout(rate=0.3)(y)
#y = layers.Dense(units=len(persons))(y)

model = Model(inputs=input,outputs=y)
optimizer = optimizers.Adam(lr=0.01)

def loss_fn(y_true,y_pred):
    print(y_pred)
    return K.categorical_crossentropy(y_true,y_pred)


#model.compile(optimizer=sgd,loss='categorical_crossentropy',metrics=['categorical_accuracy'])
model.compile(optimizer=optimizer,loss=losses.categorical_crossentropy,metrics=['categorical_accuracy'])

model.summary()
continue_learning = False

if continue_learning:
    model = load_model('../data/model.keras')

# model.fit_generator(datagen.flow(X_train, y_train, batch_size=32)
#                                  #,save_to_dir='../data/vidtest/augmented',save_prefix='gen')
#                     ,steps_per_epoch=len(X_train) / 32, epochs=20
#                     ,validation_data=(X_val,y_val),use_multiprocessing=False
#                     )


model.fit(X_train,y_train,epochs=15,validation_split=0.2,batch_size=64)
model.save('../data/model.keras')
cv2.destroyAllWindows()

