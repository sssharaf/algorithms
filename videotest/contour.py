import numpy as np
import cv2

img = cv2.imread('../data/sharaf_parmee-1.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
edge = cv2.Canny(gray, 50, 150, apertureSize=3, L2gradient=True)
blur = cv2.bilateralFilter(edge, 9, 75, 75)
ret, thresh = cv2.threshold(blur, 20, 255, 0)
image, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
img_w_cnt = cv2.drawContours(img, contours, -1, (0, 255, 0), 4)
print(len(hierarchy))
cv2.imshow('image',img_w_cnt)
cv2.waitKey(0)
cv2.destroyAllWindows()