import numpy as np
import cv2
import os
import dlib
import imutils
from imutils import face_utils

ffd = dlib.get_frontal_face_detector()

class FacePictureTaker:

    def calc_box( self, rect,f,max_row,max_col):
        (x, y, w, h) = face_utils.rect_to_bb(rect)
        x = int(x * f)
        y = int(y * f)
        w = int(w * f)
        h = int(h * f)

        wf = int(w*0.2)
        hf = int(h *0.2)
        return ( max(0,x-wf), max(0,y-hf), min(max_col,w+2*wf), min( max_row, h+2*hf))


    def take_save_picture(self,frame,person):

        dirname = '../data/vidtest/%s'%(person)
        if not os.path.exists(dirname):
           os.makedirs(dirname)
        cnt=0
        rects = None

        rows, cols = frame.shape[0],frame.shape[1]
        frame = cv2.warpAffine(frame,np.float32([[-1,0,cols],[0,1,0]]),dsize=(cols,rows))

        image = cv2.resize(frame,None,fx=0.5,fy=0.5,interpolation=cv2.INTER_AREA)
        rects = ffd(image,1)

        if(len(rects)>0):
            filename =('%s/%s.png'%(dirname,cnt))
            print("Writing to file : %s"%(filename))
            (x, y, w, h) = self.calc_box(rects[0], 2,rows,cols)
            crp = frame[y:y+h,x:x+w,:]
            cv2.imwrite(filename,crp)

        return [self.calc_box(rect,2,rows,cols) for rect in rects]

