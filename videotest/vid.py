import numpy as np
import cv2

cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #gray = frame
    gray1=cv2.Canny(gray,20,150,apertureSize=3,L2gradient=True)
    blur = cv2.bilateralFilter(gray1, 9, 75, 75)
    blur = np.uint8(np.absolute(blur))
    res = cv2.resize(gray, None, fx=1.5, fy=1.5, interpolation=cv2.INTER_CUBIC)
    # Display the resulting frame

    rows, cols = res.shape
    print(res.shape)
    flip = cv2.warpAffine(res,np.float32([[-1,0,cols],[0,1,0]]),dsize=(cols,rows))

    #imgray = cv2.cvtColor(flip, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(flip, 60, 255, 0)
    image, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    img = cv2.drawContours(image, contours, -1, (0, 255, 0), 3)

    cv2.imshow('frame',img)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()