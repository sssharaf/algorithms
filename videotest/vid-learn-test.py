import numpy as np
import cv2
import pickle
import dlib
from imutils import face_utils
from keras.models import load_model
import videotest.FacePictureTaker as FacePictureTaker

def calc_box(rect,f,max_row,max_col):
    (x, y, w, h) = face_utils.rect_to_bb(rect)
    x = int(x * f)
    y = int(y * f)
    w = int(w * f)
    h = int(h * f)

    wf = int(w*0)
    hf = int(h *0)
    return ( max(0,x-wf), max(0,y-hf), min(max_col,w+2*wf), min( max_row, h+2*hf))

cap = cv2.VideoCapture(0)
model = load_model('../data/model.keras')
y_le = pickle.load(open('../data/y_le.pickle','rb'))
ffd = dlib.get_frontal_face_detector()
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    rows, cols = frame.shape[0], frame.shape[1]

    frame = cv2.warpAffine(frame, np.float32([[-1, 0, cols], [0, 1, 0]]), dsize=(cols, rows))
    image = cv2.resize(frame, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
    #image=frame
    rects = ffd(image, 1)

    if(len(rects) > 0):
        for rect in rects:
            (x, y, w, h) = calc_box(rect, 2, rows, cols)
            crp = frame[y:y + h, x:x + w, :]
            crp = cv2.resize(crp,(250,250))
            crp = np.expand_dims(crp, 0)
            pred = model.predict(crp)
            max_idx = np.argmax(pred)
            p_person = y_le.inverse_transform(max_idx)
            cv2.putText(frame,p_person,(x,y-10),fontFace=cv2.FONT_HERSHEY_PLAIN,fontScale=3,color=(0,0,255))
            print(pred)
            print(p_person)
            print(x,y,w,h)
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

    cv2.imshow('frame', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()