import numpy as np
import cv2
import dlib
import videotest.vidutils as vutils
import imutils.face_utils as face_utils
from imutils.face_utils import FaceAligner
def shape_to_np(shape, dtype="int"):
    # initialize the list of (x, y)-coordinates
    coords = np.zeros((68, 2), dtype=dtype)

    # loop over the 68 facial landmarks and convert them
    # to a 2-tuple of (x, y)-coordinates
    for i in range(0, 68):
        coords[i] = (shape.part(i).x, shape.part(i).y)

    # return the list of (x, y)-coordinates
    return coords


cap = cv2.VideoCapture(0)
ffd = dlib.get_frontal_face_detector()
predictor =dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')
facerec = dlib.face_recognition_model_v1('dlib_face_recognition_resnet_model_v1.dat')
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = frame#cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #gray = cv2.resize(gray, None, fx=1, fy=1.5, interpolation=cv2.INTER_AREA)

    rects = ffd(gray, 1)

    for rect in rects:
        shapes = predictor(gray,rect)
        fd = facerec.compute_face_descriptor(gray, shapes)
        print(fd.shape[0])
        # x,y,w,h = face_utils.rect_to_bb(rect)
        # fa = FaceAligner(predictor, desiredFaceWidth=w,desiredFaceHeight=h)
        # gray1 = np.copy(gray)
        # gray= fa.align(gray,gray,rect)
        #
        # gray1[y:y+h,x:x+w]= gray

        #gray = cv2.bitwise_or(gray,aligned)

        # left_eye = vutils.extract_left_eye_center(shapes)
        # right_eye = vutils.extract_right_eye_center(shapes)
        # M = vutils.get_rotation_matrix(left_eye, right_eye)
        # rotated = cv2.warpAffine(img, M, (s_width, s_height), flags=cv2.INTER_CUBIC)

        # shapes = shape_to_np(shapes)
        # for (x, y) in shapes:
        #     cv2.circle(gray, (x, y), 3, (255, 0, 0), -1)
        # print(shapes)

    cv2.imshow('frame',gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()


