import numpy as np
import cv2
import os
import pandas as pd
import keras.layers as layers
from keras.models import Model
from sklearn.preprocessing import OneHotEncoder,LabelEncoder
import pickle
from keras.utils.np_utils import to_categorical
import keras.activations as activations
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
import keras.optimizers as optimizers
from keras.models import load_model
from keras import backend as K
import keras.losses as losses
import dlib
import nearpy
from nearpy.distances import EuclideanDistance
from nearpy.filters import NearestFilter
from imutils.face_utils import rect_to_bb
from nearpy.hashes import RandomBinaryProjections
import pickle
import winsound
from types import SimpleNamespace


ffd = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')
facerec = dlib.face_recognition_model_v1('dlib_face_recognition_resnet_model_v1.dat')

def sound_alarm(*args):
    image1,person = args
    print(image1.shape)
    cv2.putText(image1, 'Run! %s is here.' % (person), (0,20),
                fontFace=cv2.FONT_HERSHEY_PLAIN, fontScale=2
                , color=(0,0,255))
    # winsound.Beep(100,200)
    # winsound.Beep(500,400)
    return image1

def draw_random_circles(*args):
    image1,person,(x,y,w,h) = args
    x_b = np.random.random_integers(x,x+w,30)
    y_b = np.random.random_integers( max(0,y-100),max(0,y), 30)
    radiuses = np.random.random_integers(1,6,30)
    for x,y,radius in zip(x_b,y_b,radiuses):
        b,g,r = np.random.random_integers(0,255,3)
        color = (int(b),int(g),int(r))
        cv2.circle(image1,(x,y),radius,color)
    # winsound.Beep(100,200)
    # winsound.Beep(500,400)
    return image1

# Configurations
scale=1
read_from_pickle=True
persons = ['sharaf','parmee']

actions_per_person={}
actions_per_person['sharaf']=draw_random_circles
actions_per_person['parmee']=draw_random_circles

# Configurations
def create_engine():

    dirnames = { p:'../data/vidtest/%s'%(p) for p in persons}

    noi = 100
    engine = nearpy.Engine(128,lshashes=[RandomBinaryProjections('custom',5)]
                           ,distance=EuclideanDistance(),vector_filters=[NearestFilter(2)])

    for cnt in range(0,noi):
        for p in persons:
            fn = '%s/%s.png'%(dirnames[p],cnt)
            print("Loading file:%s"%(fn))
            img = cv2.imread(fn)
            rects = ffd(img, 1)
            if len(rects) > 0:
                rect = rects[0]
                shape=predictor(img,rect)
                fd = facerec.compute_face_descriptor(img, shape)
                fd = np.reshape(fd,(-1,))
                engine.store_vector(fd,data=p)
                #print("Storing vector for person %s.(%s)"%(p,fd))
    pickle.dump(engine,open('../data/nearpy_engine.pickle','wb'))

    return engine

engine = None
ns= None

# Main program
if read_from_pickle:
    engine = pickle.load(open('../data/nearpy_engine.pickle','rb'))
else:
    engine = create_engine()

cap = cv2.VideoCapture(0)
fnum=0
nss= None
#cv2.namedWindow('frame', cv2.WINDOW_GUI_NORMAL)
while(True):
    fnum = fnum +1

    # Capture frame-by-frame
    ret, image = cap.read()
    rows,cols = image.shape[0],image.shape[1]
    image = cv2.warpAffine(image, np.float32([[-1, 0, cols], [0, 1, 0]]), dsize=(cols, rows))

    if nss is not None:
        for ns in nss:
            x, y, w, h = ns.bb
            data, distance, recognize = ns.distance
            color_tuple = ns.color_tupple
            if recognize:
                cv2.putText(image, '%s(%0.3f)' % (data, distance), (x, y - 10),
                            fontFace=cv2.FONT_HERSHEY_PLAIN, fontScale=1
                            , color=color_tuple)

                if data in actions_per_person:
                    handler = actions_per_person[data]
                    image_ret = handler(image, data, (x, y, w, h))

            cv2.rectangle(image, (x, y), (x + w, y + h), color_tuple, 1)


    cv2.imshow('frame', image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    if fnum %5 !=0:
        continue

    frame = cv2.resize(image, None, fx=scale, fy=scale, interpolation=cv2.INTER_AREA)

    rects = ffd(frame, 1)
    nss = []
    if len(rects) > 0:

        for rect in rects:
            shape = predictor(frame, rect)
            fd = facerec.compute_face_descriptor(frame, shape)
            fd = np.reshape(fd, (-1,))
            #print("fd=%s"%fd)
            results = engine.neighbours(fd)
            #print('len(results)=%s'%len(results))
            if len(results)  > 0:
                v,data,distance =results[0]
                recognize = distance < 0.45
                x,y,w,h = rect_to_bb(rect)
                x= int(x*(1/scale))
                y= int(y*(1/scale))
                w= int(w*(1/scale))
                h =int(h*(1/scale))
                color_tuple = None
                if recognize:
                    color_tuple = (0, 255, 0)
                    # cv2.putText(image, '%s(%0.3f)' % (data, distance), (x, y - 10),
                    #             fontFace=cv2.FONT_HERSHEY_PLAIN, fontScale=1
                    #             ,color=color_tuple)
                       #image = image_ret if image_ret is not None else image
                else:
                    color_tuple = (0,0,255)

                # cv2.rectangle(image, (x, y), (x + w, y + h), color_tuple, 1)
                ns = SimpleNamespace()
                ns.bb=(x,y,w,h)
                ns.distance=(data, distance,recognize)
                ns.color_tupple=color_tuple
                nss.append(ns)

    # cv2.namedWindow('frame',cv2.WINDOW_GUI_NORMAL)
    # cv2.imshow('frame', image)


# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()


