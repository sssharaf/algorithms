import numpy as np
import cv2
import os
import dlib
import imutils
from imutils import face_utils

def calc_box(rect,f,max_row,max_col):
    (x, y, w, h) = face_utils.rect_to_bb(rect)
    x = int(x * f)
    y = int(y * f)
    w = int(w * f)
    h = int(h * f)

    wf = int(w*0.2)
    hf = int(h *0.2)
    return ( max(0,x-wf), max(0,y-hf), min(max_col,w+2*wf), min( max_row, h+2*hf))

cap = cv2.VideoCapture(0)

person = input('Enter your name: ')
dirname = '../data/vidtest/%s'%(person)
if not os.path.exists(dirname):
   os.makedirs(dirname)
start = False
cnt=0
rects = None
fNum=0
ffd = dlib.get_frontal_face_detector()
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    rows, cols = frame.shape[0],frame.shape[1]
    frame = cv2.warpAffine(frame,np.float32([[-1,0,cols],[0,1,0]]),dsize=(cols,rows))
    fNum = fNum+1
    if fNum % 3 ==0:

        fNum = 0

        if(start):
            filename =('%s/%s.png'%(dirname,cnt))
            print("Writing to file : %s"%(filename))
            crp = cv2.resize(frame,(200,200),interpolation=cv2.INTER_AREA)
            cv2.imwrite(filename,crp)
            cnt = cnt+1

    if cnt > 200:
        print("Took 500 pictures. Breaking")
        break

    cv2.imshow('frame',frame)
    key = cv2.waitKey(1)
    if key & 0xFF == ord('q'):
        break
    elif key & 0xFF == ord('t'):
        print("Taking pictures")
        start=True


# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()



